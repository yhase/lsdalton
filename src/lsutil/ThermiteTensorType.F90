!> Module for internal integral and matrix storage and operations
MODULE ThermiteTensor_type
use precision

public :: tt_type,tt_contraction

!> The maximal dimension of the tt-tensor
integer,parameter :: max_AOdims=10

!*******                AOtype            *******
!> Empty means in practice one lower dimension
Integer,parameter :: AOtype_Empty = 0
!> AO mean that it is a regular Gaussian AO index
Integer,parameter :: AOtype_AO    = 1 
!> dim5 means a joint index for derivative and operator components as well as for various contractions
Integer,parameter :: AOtype_dim5  = 2

!*******                patype            *******
!Partitioning schemes are used when considering a subblock of a full tensor
!> Full means the full set of orbitals
Integer,parameter :: patype_full = 1
!> Partition the dimension from a subset of atoms
Integer,parameter :: patype_atom_partition = 2
!> Partition the dimension from a subset of batches
Integer,parameter :: patype_batch_partition = 3
!> Means no partitioning possible
Integer,parameter :: patype_none = 4

!*******                elms_status       *******
!> Elms not yet assigned or allocated
Integer,parameter :: elms_status_unassigned = 1
!> Indicates that the elms are pointing to memory allocated externally
Integer,parameter :: elms_status_assigned = 2
!> Indicates that the elms are allocated internally
Integer,parameter :: elms_status_allocated = 3  

!> Contrains information about each dimension of the TT_TYPE
TYPE TT_AO_info
  !> Specfies the dimension type, from list of AOtype_xxx above
  Integer :: AOtype      
  !> Specfies the partition type, from list of patype_xxx above
  Integer :: patype
  !> The number of orbitals
  Integer :: norb      
  !> The number of atoms
  Integer :: natoms     
  !> The number of batches
  Integer :: nbatches  
  !> The maximal number of angular momenta shells per batch (for familiy/"shared exponent"-type basis)
  Integer :: nAngmom_max  
  !> Specifies if the pointers below have been allocated
  Logical :: allocated
  !> The number of orbitals for each atom, dimension natoms
  Integer,pointer :: atom_num_orb(:)
  !> The starting orbital for each atom, dimension natoms
  Integer,pointer :: atom_start_orb(:)
  !> The number of orbitals for each batch, dimension nbatches,nAngmom
  Integer,pointer :: batch_num_orb(:,:)
  !> The starting orbital for each batch, dimension nbatches,nAngmom
  Integer,pointer :: batch_start_orb(:,:)
  !> The atom the batch belongs to, dimension nbatches
  Integer,pointer :: batch_atom(:)
  !> The number of angular momentums for the batch (family basis sets with shared exponents), dimension nbatches
  Integer,pointer :: batch_nAngmom(:)

  !*******                For patype_atom_partition ONLY            *******
  !> List of global atoms in partition, dimension natoms
  Integer,pointer :: pa_atom(:)

  !*******                For patype_batch_partition ONLY            *******
  !> List of global batches in partition, dimension nbatches
  Integer,pointer :: pa_batch(:)
END TYPE TT_AO_info

TYPE TT_AO_info_pt
  TYPE(TT_AO_info),pointer :: p
END TYPE TT_AO_info_pt

TYPE TT_TYPE
  !> The dimensionality of the tensor
  Integer                      :: ndim
  !> Orbital information for each dimension
  Type(TT_AO_info_pt),pointer :: AO_info(:)
  !> the total number of elements, *= AO_info(i)%ndim
  Integer                     :: nelms
  !> The tensor elements
  Real(realk),pointer         :: elms(:)
  !> Indicates whether the elements are unassigned, assigned or allocated
  Integer                     :: elms_status
END TYPE TT_TYPE

!> Specifies on single contraction
TYPE TT_CONT
  !> Tensor to contract with
  TYPE(TT_TYPE)   :: TT
  !> Number of indeces to contract over (equal to the dimensionality of the tensor)
  Integer         :: nIndeces
  !> The contraction indeces pattern
  Integer,pointer :: indeces(:)
END TYPE TT_CONT

!> Specifies on sequence of contractions
TYPE TT_CONT_RULE
  !> The multiplicicative factor for the contraction
  real(realk)           :: fac
  !> The number of tensors to contract with
  integer               :: nTensors
  !> The contractions
  TYPE(TT_CONT),pointer :: TT_CONT(:) !dimension nTensors
  !> Optional name for printing
  Character(len=80)     :: txt
END TYPE TT_CONT_RULE

!> Contraction rules. We contract with integrals of type <ab|w|cd>^x, with 
!> w the operator, x the derivative component with index 5, and a,b,c,d are AOs with index 1-4
!> Take as an example the Coulomb and exchange matrix contribution:
!>
!>      1. we need to set up two contractions, nRules = 2
!>      2. for closed shells the Coulomb factor should be 2.0, TT_rule(1)%fac=2.0, and we should contract
!>         with one tensor (the density matrix) with index 3 and 4, i.e. TT_rule(1)%nTensors=1, 
!>         TT_rule(1)%TT_CONT(1)%nIndeces = 2 and TT_rule(1)%TT_CONT(1)%indeces = (/ 3,4 /)
!>         Note that for contraction with multiple matrices we could add a dimension to the contraction
!>         tensor and contract with 3 rather than 2 indeces, say TT_rule(1)%TT_CONT(1)%indeces = (/ 3,4,6 /)
!>         where the last index (>5) would have dimension equal to the number of density matrices
!>      3. for exchange the exchange factor should be used: TT_rule(1)%fac=-setting%exchangeFactor. Here 
!>         everything follows the same pattern as for Coulomb, except now TT_rule(1)%TT_CONT(1)%indeces = (/ 2,4 /)
!>      4. We will generate 2 output tensors, containing the Coulomb and exchange matrix, with add=.FALSE. 
!>         or the combined output tensor with add=.TRUE.
!>
TYPE TT_CONTRACTION
  !> Output tensors, one for each rule unless the flag add is set to true (where there will only be one output)
  TYPE(TT_TYPE),pointer        :: Output(:)
  !> The number of contraction rules
  Integer                      :: nRules
  !> The contraction rules
  TYPE(TT_CONT_RULE),pointer   :: TT_rule(:)
  !> Specifies if the different contraction rules should combine to the same output or not
  Logical                      :: add
END TYPE TT_CONTRACTION

END MODULE ThermiteTensor_type
