!> @file 
!> contains a Matrix used to save info for scalapack
MODULE lsutil_ScalapackMod
 use precision
 use matrix_module
 use matrix_operations
 use matrix_operations_scalapack
 use LSTENSOR_OPERATIONSMOD
 use LSTENSOR_TYPETYPE
 use infpar_module
 use AO_typetype
 use lsmpi_param
 use lsparameters
 use memory_handling
 use lsmpi_Bcast 
 use lsmpi_ReductionMod
 use lsmpi_typeParam
private
 
 public :: lsutil_Scalapack_init_master,&
      & lsutil_Scalapack_init_slave,&
      & lsutil_scalapack_retrieve,&
      & lsutil_lstensor_to_scalapack,&
      & lsutil_lstensor_to_scalapackACC,&
      & lsutil_Scalapack_print_master,&
      & lsutil_Scalapack_print_slave,&
      & lsutil_symMat_from_triangularMat,&
      & lsutil_scalapack_build_lst_from_matarray

 INTERFACE lsutil_scalapack_retrieve
    MODULE PROCEDURE lsutil_scalapack_retrieve1,&
         & lsutil_scalapack_retrieve2
 END INTERFACE lsutil_scalapack_retrieve

 type(matrix),pointer,save :: TmpScalapackMat(:) !FIXME NEED TO EXPAND TO ARRAY
 logical :: TmpScalapackMatAllocated
 integer :: nTmpScalapackMat
 type(matrix):: TmpScalapackMatDummy
Contains

subroutine lsutil_scalapack_init_master(n1,n2,nmat)
implicit none
integer,intent(in) :: n1,n2,nmat
!
integer :: I
#ifdef VAR_MPI
#ifdef VAR_SCALAPACK  
TmpScalapackMatAllocated = .TRUE.
nTmpScalapackMat = nmat
call mem_alloc(TmpScalapackMat,nmat)
do I=1,nmat
   call mat_nullify(TmpScalapackMat(I))
   call mat_set_magic_tag(TmpScalapackMat(I))
   call mat_scalapack_init(TmpScalapackMat(I),n1,n2)
   call mat_scalapack_zero(TmpScalapackMat(I))
enddo
#else
call lsquit('lsutil_scalapack_init_master require ScaLapack',-1)
#endif
#endif
end subroutine lsutil_scalapack_init_master

subroutine lsutil_scalapack_init_slave(nmat)
implicit none
integer,intent(in) :: nmat
!
integer :: job,I
#ifdef VAR_MPI
#ifdef VAR_SCALAPACK  
TmpScalapackMatAllocated = .TRUE.
nTmpScalapackMat = nmat
do I=1,nmat
   !call mat_scalapack_init(TmpScalapackMat,n1,n2)
   call ls_mpibcast(job,infpar%master,MPI_COMM_LSDALTON)
   IF(PDMSLAVE.NE.job)call lsquit('MPI sync error1 in lsutil_scalapack_init_slave',-1)
   IF(scalapack_member)THEN   
      call PDM_SLAVE()
   ENDIF

   !call mat_scalapack_zero(TmpScalapackMat)
   call ls_mpibcast(job,infpar%master,MPI_COMM_LSDALTON)
   IF(PDMSLAVE.NE.job)call lsquit('MPI sync error2 in lsutil_scalapack_init_slave',-1)
   IF(scalapack_member)THEN   
      call PDM_SLAVE()
   ENDIF
enddo
#else
call lsquit('lsutil_scalapack_init_slave require ScaLapack',-1)
#endif
#endif
end subroutine lsutil_scalapack_init_slave

subroutine lsutil_scalapack_retrieve1(A)
implicit none
type(matrix) :: A
#ifdef VAR_MPI
#ifdef VAR_SCALAPACK  
IF(TmpScalapackMatAllocated)THEN
   IF(nTmpScalapackMat.NE.1)THEN
      call lsquit('dim mismatch LSUTIL_SCALAPACK_RETRIEVE',-1)
   ENDIF
   call mat_assign(A,TmpScalapackMat(1))
   call mat_free(TmpScalapackMat(1))
   call mem_dealloc(TmpScalapackMat)
   TmpScalapackMatAllocated = .FALSE.
ELSE
   call lsquit('LSUTIL_SCALAPACK_RETRIEVE1 error ',-1)
ENDIF
#else
call lsquit('LSUTIL_SCALAPACK_RETRIEVE1 require ScaLapack',-1)
#endif
#endif
end subroutine LSUTIL_SCALAPACK_RETRIEVE1

subroutine lsutil_scalapack_retrieve2(A)
implicit none
type(matrix) :: A(nTmpScalapackMat)
!
integer :: I
#ifdef VAR_MPI
#ifdef VAR_SCALAPACK  
IF(TmpScalapackMatAllocated)THEN
   DO I=1,nTmpScalapackMat
      call mat_assign(A(I),TmpScalapackMat(I))
      call mat_free(TmpScalapackMat(I))
   ENDDO
   call mem_dealloc(TmpScalapackMat)
   TmpScalapackMatAllocated = .FALSE.
ELSE
   call lsquit('LSUTIL_SCALAPACK_RETRIEVE2 error ',-1)
ENDIF
#else
call lsquit('LSUTIL_SCALAPACK_RETRIEVE2 require ScaLapack',-1)
#endif
#endif
end subroutine LSUTIL_SCALAPACK_RETRIEVE2

subroutine lsutil_lstensor_to_scalapack(tensor,mynum,comm,II,JJ,PermuteResultTensor)
implicit none
type(lstensor) :: tensor
INTEGER(kind=ls_mpik),intent(in)   :: comm,mynum
integer :: II,JJ !index usually 1 and 2 
logical :: PermuteResultTensor
!
integer :: TMP(tensor%nLSAO),I,n1,n2,maxBat,maxAng,s1,s2,mynum2,job,S1A,S2A,J
TYPE(LSAOTENSOR),pointer    :: lsao
REAL(REALK),pointer     :: elms(:)
REAL(REALK),pointer     :: elms2(:,:,:)
integer :: DIM,nelms,IATOM1,IATOM2
integer :: nbat1,nbat2,Jbat,Ibat,offset2,offset1,JORB
integer :: Iangmom,Jangmom,nOrbA,nOrbB,sA,sB,IMAT,IELM,IORB
integer :: II2,JJ2
#ifdef VAR_MPI
#ifdef VAR_SCALAPACK  
!call lsmpi_barrier(scalapack_comm)
!IF(infpar%mynum.EQ.0)THEN
!   print*,'XXX PRINT GLOBAL BEFORE :  '
!   call mat_scalapack_print_global(TmpScalapackMat)
!ELSE
!   call ls_mpibcast(job,infpar%master,MPI_COMM_LSDALTON)
!   IF(PDMSLAVE.NE.job)call lsquit('MPI sync error2 in lsutil_scalapack_init_slave',-1)
!   IF(scalapack_member)THEN   
!      call PDM_SLAVE()
!   ENDIF
!ENDIF

!call lsmpi_barrier(scalapack_comm)

DO I=1,tensor%nLSAO   
   TMP(I) = 0
ENDDO
MYNUM2 = MYNUM
DO I=1,tensor%nLSAO   
   IF(tensor%LSAO(I)%GLSAOindex.NE.0)TMP(I) = MYNUM2
ENDDO
!call lsmpi_reduction(TMP,tensor%nLSAO,infpar%master,comm)
!call ls_mpibcast(TMP,tensor%nLSAO,infpar%master,comm)
call lsmpi_allreduce(TMP,tensor%nLSAO,comm)

DO I=1,tensor%nLSAO
   IF(TMP(I).EQ.infpar%mynum)THEN
      maxBat = tensor%LSAO(I)%maxBat
      maxAng = tensor%LSAO(I)%maxAng 
      s1 = tensor%LSAO(I)%startGlobalOrb(1+(II-1)*maxAng*maxBat)
      s2 = tensor%LSAO(I)%startGlobalOrb(1+(JJ-1)*maxAng*maxBat)
      IF(PermuteResultTensor)THEN
         IF(s1.GT.s2)THEN !Lower triangular part 
!            CYCLE !S1,S2 not known on the other slaves 
            II2 = JJ
            JJ2 = II
            s1 = tensor%LSAO(I)%startGlobalOrb(1+(II2-1)*maxAng*maxBat)
            s2 = tensor%LSAO(I)%startGlobalOrb(1+(JJ2-1)*maxAng*maxBat)
            n1 = tensor%LSAO(I)%nLocal(II2)
            n2 = tensor%LSAO(I)%nLocal(JJ2)
            CALL LS_DZERO(tensor%LSAO(I)%elms,n1*n2)
         ELSE !upper triangular part
            II2 = II
            JJ2 = JJ
         ENDIF
      ELSE !upper triangular part
         II2 = II
         JJ2 = JJ
      ENDIF
      n1 = tensor%LSAO(I)%nLocal(II2)
      n2 = tensor%LSAO(I)%nLocal(JJ2)


      IATOM1 = tensor%LSAO(I)%ATOM(II2)
      IATOM2 = tensor%LSAO(I)%ATOM(JJ2)
!      print*,'XXX II,JJ',II,JJ,'IATOM1,IATOM2',IATOM1,IATOM2
!      print*,'XXX n1,n2,s1,s2',n1,n2,s1,s2
      !This is my block
!      print*,'XXX nELMS:',size(tensor%LSAO(I)%elms),tensor%LSAO(I)%nelms
!      print*,'XXX GLOBAL s1,s2',s1,s2
!      print*,'XXX OWNER',TMP(I),'   ',n1,n2
!      call ls_output(tensor%LSAO(I)%elms,1,n1,1,n2,n1,n2,1,6)

!      print*,'XXX OWNER  elms:',tensor%LSAO(I)%elms
!      Then Loop over collums 
      lsao => TENSOR%LSAO(I) 
      elms => lsao%elms
      DIM = size(lsao%elms)
      nelms = lsao%nelms
!      print*,'XXX nelms',nelms,'DIM',DIM
      IF(maxAng.NE.1)THEN
         !In the case of family type basis set we need to reorder the TENSOR%LSAO(I)%elms 
         !to correspond to the family basisset orientation. 
         call mem_alloc(elms2,n1,n2,nTmpScalapackMat)
         IF(PermuteResultTensor)THEN
            IF(s1.EQ.s2)THEN 
               !the Matrix have had its lower tridiagonal part set to zero
               !when we reorder this could/will affect in a Matrix which nolonger have that 
               !property. This is only a problem on the diagonal so in this case we set
               !the lower diagnal part to the upper diagonal part and 
               !after the reordering we could set it to zero again but no need. 
               call FULL_SYMelms_FROM_TRIANGULARelms3(elms,n1,n2,nTmpScalapackMat)

!               print*,'XXX FULL_SYMelms_FROM_TRIANGULARelms3 ',n1,n2
!               call ls_output(elms,1,n1,1,n2,n1,n2,1,6)

            ENDIF
         ENDIF
         nbat1 = TENSOR%nAOBATCH(IATOM1,II2)
         nbat2 = TENSOR%nAOBATCH(IATOM2,JJ2)
         IELM = 0
         DO IMAT = 1,nTmpScalapackMat
          DO Jbat = 1,nbat2
           offset2 = (Jbat-1)*maxAng+(JJ2-1)*maxBat*maxAng
           DO Jangmom = 1,lsao%nAngmom(Jbat+(JJ2-1)*maxBat)
            nOrbB = lsao%nOrb(Jangmom+offset2)
            sB = lsao%startGlobalOrb(Jangmom+offset2)&
                 & - lsao%startGlobalOrb(1+(JJ2-1)*maxBat*maxAng)

            DO JORB = 1,nOrbB
             DO Ibat = 1,nbat1
              offset1 = (Ibat-1)*maxAng+(II2-1)*maxBat*maxAng
              DO Iangmom = 1,lsao%nAngmom(Ibat+(II2-1)*maxBat)
               nOrbA = lsao%nOrb(Iangmom+offset1)
               sA = lsao%startGlobalOrb(Iangmom+offset1)&
                    & - lsao%startGlobalOrb(1+(II2-1)*maxBat*maxAng)
               DO IORB = 1,nOrbA
                  IELM = IELM+1
                  elms2(sA+IORB,sB+JORB,IMAT) = elms(IELM)
 
               ENDDO
              ENDDO
             ENDDO
            ENDDO
           ENDDO
          ENDDO
         ENDDO
         IELM = 0
         DO IMAT = 1,nTmpScalapackMat
            DO JORB = 1,n2
               DO IORB = 1,n1
                  IELM = IELM + 1
                  elms(IELM) = elms2(IORB,JORB,IMAT)
               ENDDO
            ENDDO
         ENDDO
         call mem_dealloc(elms2)
      ENDIF
      IF(infpar%mynum.EQ.infpar%master)THEN
         IF(nTmpScalapackMat.GT.1)THEN
            DO J=1,nTmpScalapackMat
               call mat_scalapack_add_block_owner(TmpScalapackMat(J),&
                    & tensor%LSAO(I)%elms(1+(J-1)*nelms:J*nelms),n1,n2,s1,s2)
            ENDDO
         ELSE
            call mat_scalapack_add_block_owner(TmpScalapackMat(1),&
                 & tensor%LSAO(I)%elms,n1,n2,s1,s2)
         ENDIF
      ELSE
         IF(nTmpScalapackMat.GT.1)THEN
            DO J=1,nTmpScalapackMat
               call mat_scalapack_add_block_owner(TmpScalapackMatDummy,&
                    & tensor%LSAO(I)%elms(1+(J-1)*nelms:J*nelms),n1,n2,s1,s2)
            ENDDO
         ELSE
            call mat_scalapack_add_block_owner(TmpScalapackMatDummy,tensor%LSAO(I)%elms,&
                 & n1,n2,s1,s2)
         ENDIF
      ENDIF
   ELSE
      IF(infpar%mynum.EQ.infpar%master)THEN
         IF(nTmpScalapackMat.GT.1)THEN
            DO J=1,nTmpScalapackMat
               call mat_scalapack_add_block_notowner(TmpScalapackMat(J),TMP(I))
            ENDDO
         ELSE
            call mat_scalapack_add_block_notowner(TmpScalapackMat(1),TMP(I))
         ENDIF
      ELSE
         IF(nTmpScalapackMat.GT.1)THEN
            DO J=1,nTmpScalapackMat
               call mat_scalapack_add_block_notowner(TmpScalapackMatDummy,TMP(I))
            ENDDO
         ELSE
            call mat_scalapack_add_block_notowner(TmpScalapackMatDummy,TMP(I))
         ENDIF
      ENDIF
   ENDIF
ENDDO
#else
call lsquit('lsutil_lstensor_to_scalapack require ScaLapack',-1)
#endif
#endif

!call lsmpi_barrier(scalapack_comm)
!print*,'FINISHED INSIDE lsutil_lstensor_to_scalapack  MYNUM',mynum
!call lsquit('lsutil_lstensor_to_scalapack TEST DONE',-1)

end subroutine lsutil_lstensor_to_scalapack

!!$subroutine TransposeBlock(n1,n2,Mat)
!!$implicit none
!!$integer,intent(in) :: n1,n2
!!$real(realk),intent(inout) :: Mat(n2*n1)
!!$real(realk) :: Tmp(n1,n2)
!!$!
!!$integer :: I,J
!!$!Transpose
!!$do J=1,n2
!!$   do I=1,n1 
!!$      Tmp(I,J) = Mat(J+(I-1)*n2)
!!$   enddo
!!$enddo
!!$!copy
!!$do J=1,n2
!!$   do I=1,n1 
!!$      Mat(I+(J-1)*n1) = Tmp(I,J)
!!$   enddo
!!$enddo
!!$
!!$end subroutine TransposeBlock
            
!All nodes may have contribution to the same block Therefore this version
!builds a block containing several atoms (ndimA,ndimB < BLOCK_SIZE**2)
!Performs a reduction on the block followed by a add_block
subroutine lsutil_lstensor_to_scalapackACC(tensor,mynum,comm,II,JJ,PermuteResultTensor)
implicit none
type(lstensor) :: tensor
INTEGER(kind=ls_mpik),intent(in)   :: comm,mynum
integer :: II,JJ
logical :: PermuteResultTensor
!
integer :: TMP(tensor%nLSAO),I,n1,n2,maxBat,maxAng,s1,s2,mynum2,job,S1A,S2A,J
TYPE(LSAOTENSOR),pointer    :: lsao
REAL(REALK),pointer     :: elms(:)
REAL(REALK),pointer     :: elms2(:,:,:),TmpBlock(:,:,:)
integer :: DIM,nelms
integer :: nbat1,nbat2,Jbat,Ibat,offset2,offset1,JORB
integer :: Iangmom,Jangmom,nOrbA,nOrbB,sA,sB,IMAT,IELM,IORB
integer :: IIn(4),nbastII,JATOM,ndimJ,IATOM
#ifdef VAR_MPI
#ifdef VAR_SCALAPACK  
!call lsmpi_barrier(scalapack_comm)
IIn(1)=1
IIn(2)=1
IIn(3)=1
IIn(4)=1

nbastII = TENSOR%nBast(II) 
s2 = 1
DO JATOM = 1,TENSOR%nAtom(JJ)
   IIn(JJ)=JATOM
   ndimJ = TENSOR%AO(JJ)%p%ATOMICnOrb(JATOM)

   !Build Block(nbastII,ndimJ)
   call mem_alloc(TmpBlock,nbastII,ndimJ,nTmpScalapackMat)
   DO IMAT = 1,nTmpScalapackMat
      DO JORB = 1,ndimJ
         DO IORB = 1,nbastII
            TmpBlock(IORB,JORB,IMAT) = 0.0E0_realk
         ENDDO
      ENDDO
   ENDDO
   DO IATOM = 1,TENSOR%nAtom(II) 
      IIn(II)=IATOM
      I = TENSOR%INDEX(IIn(1),IIn(2),IIn(3),IIn(4))
      IF(I.NE.0)THEN
         lsao => TENSOR%LSAO(I) 
         elms => lsao%elms
         maxBat = tensor%LSAO(I)%maxBat
         maxAng = tensor%LSAO(I)%maxAng 
         s1 = tensor%LSAO(I)%startGlobalOrb(1+(II-1)*maxAng*maxBat)
         n1 = tensor%LSAO(I)%nLocal(II)

         DIM = size(lsao%elms)
         nelms = lsao%nelms
         IF(maxAng.NE.1)THEN
            !In the case of family type basis set we need to reorder the TENSOR%LSAO(I)%elms 
            !to correspond to the family basisset ordering
            nbat1 = TENSOR%nAOBATCH(IATOM,II)
            nbat2 = TENSOR%nAOBATCH(JATOM,JJ)
            
            IELM = 0
            DO IMAT = 1,nTmpScalapackMat
             DO Jbat = 1,nbat2
              offset2 = (Jbat-1)*maxAng+(JJ-1)*maxBat*maxAng
              DO Jangmom = 1,lsao%nAngmom(Jbat+(JJ-1)*maxBat)
               nOrbB = lsao%nOrb(Jangmom+offset2)
               sB = lsao%startGlobalOrb(Jangmom+offset2)&
                    & - lsao%startGlobalOrb(1+(JJ-1)*maxBat*maxAng)
               DO JORB = 1,nOrbB
                DO Ibat = 1,nbat1
                 offset1 = (Ibat-1)*maxAng+(II-1)*maxBat*maxAng
                 DO Iangmom = 1,lsao%nAngmom(Ibat+(II-1)*maxBat)
                  nOrbA = lsao%nOrb(Iangmom+offset1)
                  sA = lsao%startGlobalOrb(Iangmom+offset1)&
                       & - lsao%startGlobalOrb(1+(II-1)*maxBat*maxAng)
                  DO IORB = 1,nOrbA
                   IELM = IELM+1
                   TmpBlock(s1-1+(sA+IORB),sB+JORB,IMAT) = elms(IELM)
                  ENDDO
                 ENDDO
                ENDDO
               ENDDO
              ENDDO
             ENDDO
            ENDDO
         ELSE
            IELM=0
            DO IMAT = 1,nTmpScalapackMat
             DO JORB = 1,ndimJ
              DO IORB = 0,n1-1
                 IELM = IELM + 1
                 TmpBlock(s1+IORB,JORB,IMAT) = elms(IELM)
              ENDDO
             ENDDO
            ENDDO
         ENDIF
      ENDIF
   ENDDO
   !Reduction
   !FIXME WARNING WILL NOT WORK WITH AUTO

   !=====================================================================
!   call sleep(5*infpar%mynum)
!   print*,'LOCAL TmpBLOCK(',nbastII,',',ndimJ,',JATOM=',JATOM,')   MYNUM',infpar%mynum
!   call ls_output(TmpBlock,1,nbastII,1,ndimJ,nbastII,ndimJ,1,6)
   !=====================================================================
   call lsmpi_reduction(TmpBlock,nbastII,ndimJ,nTmpScalapackMat,&
        & infpar%master,scalapack_comm)

   IF(infpar%mynum.EQ.infpar%master)THEN
      s1 = 1
      IF(nTmpScalapackMat.GT.1)THEN
         DO J=1,nTmpScalapackMat
            call mat_scalapack_add_block_owner(TmpScalapackMat(J),TmpBlock,nbastII,ndimJ,s1,s2)
         ENDDO
      ELSE
         call mat_scalapack_add_block_owner(TmpScalapackMat(1),TmpBlock,nbastII,ndimJ,s1,s2)
      ENDIF
   ELSE
      IF(nTmpScalapackMat.GT.1)THEN
         DO J=1,nTmpScalapackMat
            call mat_scalapack_add_block_notowner(TmpScalapackMatDummy,infpar%master)
         ENDDO
      ELSE
         call mat_scalapack_add_block_notowner(TmpScalapackMatDummy,infpar%master)
      ENDIF
   ENDIF
   call mem_dealloc(TmpBlock)   
   s2 = s2 + ndimJ
ENDDO
#else
call lsquit('lsutil_lstensor_to_scalapack require ScaLapack',-1)
#endif
#endif

!call lsmpi_barrier(scalapack_comm)
!print*,'FINISHED INSIDE lsutil_lstensor_to_scalapack  MYNUM',mynum
!call lsquit('lsutil_lstensor_to_scalapack TEST DONE',-1)

end subroutine lsutil_lstensor_to_scalapackACC

subroutine FULL_SYMelms_FROM_TRIANGULARelms3(elms,dimenA,dimenB,nmat)
  implicit none
  integer :: dimenA,dimenB,nmat
  real(realk),intent(inout) :: elms(dimenA,dimenB,nmat)
  !
  integer :: A,B,IMAT
  Do IMAT = 1,NMAT
     DO B=1,dimenB
        DO A=1,B-1
           elms(B,A,IMAT) = elms(A,B,IMAT)
        ENDDO
     ENDDO
  ENDDO
END subroutine FULL_SYMELMS_FROM_TRIANGULARELMS3

subroutine lsutil_scalapack_print_master()
implicit none
#ifdef VAR_MPI
#ifdef VAR_SCALAPACK  
print*,'MASTER print mat_print:  '
call lsmpi_barrier(scalapack_comm)
call mat_scalapack_print_global(TmpScalapackMat(1))
#else
call lsquit('lsutil_scalapack_print_master require ScaLapack',-1)
#endif
#endif
end subroutine lsutil_scalapack_print_master

subroutine lsutil_scalapack_print_slave()
implicit none
integer :: job
#ifdef VAR_MPI
#ifdef VAR_SCALAPACK  
call lsmpi_barrier(scalapack_comm)

print*,'SLAVE print mat_print_global:  '
call ls_mpibcast(job,infpar%master,MPI_COMM_LSDALTON)
IF(PDMSLAVE.NE.job)call lsquit('MPI sync error2 in lsutil_scalapack_init_slave',-1)
IF(scalapack_member)THEN   
   call PDM_SLAVE()
ENDIF
#else
call lsquit('lsutil_scalapack_print_slave require ScaLapack',-1)
#endif
#endif
end subroutine lsutil_scalapack_print_slave

subroutine lsutil_scalapack_build_lst_from_matarray(lst_DRHS,DmatRHS,&
     & AO1,AO2,nbastRHS1,nbastRHS2,nDmatRHS,useAO1,useAO2,ODscreen,lupri)
implicit none
TYPE(LSTENSOR)     :: LST_DRHS
TYPE(MATRIXP)      :: DmatRHS(:) !slave do not actually have this one
TYPE(AOITEM),target :: AO1,AO2
INTEGER            :: nbastRHS1,nbastRHS2,nDmatRHS,lupri
logical   :: useAO1,useAO2,ODscreen
!local variables
#ifdef VAR_SCALAPACK
integer(kind=short) :: Thrlog
integer :: nAtoms1,nAtoms2,numnodes,nLocalBlocks,I,J,imyblock,II
integer :: nMaxRow,nMaxCol,iBlocks,nAtomsArray1,nAtomsArray2,nn2,JJ
Integer :: iLSAO,n1,n2,iglobalstartrow,iglobalstartcol,sA,sB,nn1
Integer :: RowSize,ColSize,IATOM,JATOM,IDMAT,sc,sr,maxAng,maxBat
integer(kind=short),pointer  :: AtomDrhs(:,:)
logical :: MEMDISTMATRIX,DSYM,AddSymBlock
real(realk) :: factor,MaxDmatVal
real(realk),pointer :: ATMP(:,:,:)
integer,pointer :: Rowindex(:),nRow(:),AtomsArray1(:)
integer,pointer :: Colindex(:),nCol(:),AtomsArray2(:)
integer :: DistRowLST(nbastRHS1),nDistRow
integer :: DistRowATMP(nbastRHS1),nDistCol
integer :: DistColLST(nbastRHS2)
integer :: DistColATMP(nbastRHS2)
Integer :: Jbat,Ibat,nbat2,nbat1,offset1,offset2,sA3,sB3,offsetB,offsetA
Integer :: Jangmom,nOrbB,Iangmom,nOrbA,sr2,sc2,sA2,sB2,nDistCol2,nDistRow2
integer(kind=short) :: MaxDmatValshort
!print*,'Before mat_scalapack_print_global',scalapack_comm
!print*,'MPI_COMM_LSDALTON',MPI_COMM_LSDALTON
!call lsmpi_barrier(scalapack_comm)
numnodes = infpar%nodtot
DSYM = .TRUE.
!print*,'DmatRHS(1)%localnrow',DmatRHS(1)%p%localnrow
!print*,'DmatRHS(1)%localncol',DmatRHS(1)%p%localncol
!print*,'BLOCK_SIZE          ',BLOCK_SIZE
call scalapack_nLocal_blocks(nLocalBlocks,nMaxRow,nMaxCol,nbastRHS1,nbastRHS2)
!print*,'nLocalBlocks',nLocalBlocks
call mem_alloc(ATMP,nMaxRow,nMaxCol,nLocalBlocks)
call mem_alloc(Rowindex,nLocalBlocks)
call mem_alloc(Colindex,nLocalBlocks)
call mem_alloc(nRow,nLocalBlocks)
call mem_alloc(nCol,nLocalBlocks)

!Get the blocks on this node
IF(infpar%mynum.EQ.infpar%master)THEN
   call scalapack_Local_blocks(nLocalBlocks,nMaxRow,nMaxCol,&
        & nbastRHS1,nbastRHS2,ATMP,Rowindex,Colindex,nRow,nCol,DmatRHS(1)%p)
ELSE
   call scalapack_Local_blocks(nLocalBlocks,nMaxRow,nMaxCol,&
        & nbastRHS1,nbastRHS2,ATMP,Rowindex,Colindex,nRow,nCol)
ENDIF
!call lsmpi_barrier(scalapack_comm)
!call sleep(5*infpar%mynum)
!print*,'START   ',infpar%mynum
!Build Dbd Screening matrix. 
nAtoms1 = AO1%natoms
nAtoms2 = AO2%natoms
call mem_alloc(AtomDrhs,nAtoms1,nAtoms2)
do J=1,nAtoms2
   do I=1,nAtoms1
      AtomDrhs(I,J) = shortzero
   enddo
enddo
call mem_alloc(AtomsArray1,nAtoms1)
call mem_alloc(AtomsArray2,nAtoms2)
DO imyblock=1,nLocalBlocks
   iglobalstartrow = Rowindex(imyblock)
   iglobalstartcol = Colindex(imyblock)
   RowSize = nRow(imyblock)
   ColSize = nCol(imyblock)

   MaxDmatVal = MAXVAL(ABS(ATMP(1:RowSize,1:ColSize,imyblock)))

   IF (MaxDmatVal.GT.shortintCRIT) THEN
      MaxDmatValshort = CEILING(LOG10(MaxDmatVal))
   ELSE
      MaxDmatValshort = shortzero !meaning -33=> 10**-33=0
   ENDIF

   call determineAtomsArray(RowSize,iglobalstartrow,&
        & AtomsArray1,nAtoms1,nAtomsArray1,AO1)

   call determineAtomsArray(ColSize,iglobalstartcol,&
        & AtomsArray2,nAtoms2,nAtomsArray2,AO2)

!   print*,'AtomsArray1:',AtomsArray1(1:nAtomsArray1),'   nAtomsArray1',nAtomsArray1
!   print*,'AtomsArray2:',AtomsArray2(1:nAtomsArray2),'   nAtomsArray2',nAtomsArray2   
   DO J=1,nAtomsArray2
      JATOM = AtomsArray2(J)
      DO I=1,nAtomsArray1
         IATOM = AtomsArray1(I)
         AtomDrhs(IATOM,JATOM) = MAX(AtomDrhs(IATOM,JATOM),MaxDmatValshort)
         IF(DSYM)AtomDrhs(JATOM,IATOM) = MAX(AtomDrhs(JATOM,IATOM),MaxDmatValshort)
!         AtomDrhs(IATOM,IATOM) = MaxDmatValshort 
!         AtomDrhs(JATOM,JATOM) = MaxDmatValshort
      ENDDO
   ENDDO
ENDDO

Thrlog = -20  !screen away Dmat elements smaller than 10^-20 

MEMDISTMATRIX = .TRUE.
call init_lstensor_5dim(lst_DRHS,AO1,AO2,AO1,AO2,nbastRHS1,nbastRHS2,1,1,nDmatRHS,&
     & .TRUE.,.TRUE.,.FALSE.,.FALSE.,ODscreen,.FALSE.,lupri,MEMDISTMATRIX)

!print*,'AtomDrhs Thrlog',Thrlog,' MYNUM',infpar%mynum
!call shortint_output(AtomDrhs,nAtoms1,nAtoms2,6)

!call lsmpi_barrier(scalapack_comm)
!call sleep(5*infpar%mynum)

!2. Init LSTENSOR Using Dbd Screening matrix (like kac !) 
call lstensor_alloc_kac_memdist(lst_DRHS,AO1,AO2,nAtoms1,nAtoms2,AtomDrhs,Thrlog)
call mem_dealloc(AtomDrhs)

DO IDMAT=1,nDmatRHS
 IF(IDMAT.GT.2)THEN
    IF(infpar%mynum.EQ.infpar%master)THEN
       call scalapack_Local_blocks(nLocalBlocks,nMaxRow,nMaxCol,&
            & nbastRHS1,nbastRHS2,ATMP,Rowindex,Colindex,nRow,nCol,DmatRHS(IDMAT)%p)
    ELSE
       call scalapack_Local_blocks(nLocalBlocks,nMaxRow,nMaxCol,&
            & nbastRHS1,nbastRHS2,ATMP,Rowindex,Colindex,nRow,nCol)
    ENDIF
 ENDIF
 
 !3. Loop over Blocks and add atomic blocks to LST  
 DO imyblock=1,nLocalBlocks
      
  iglobalstartrow = Rowindex(imyblock)
  iglobalstartcol = Colindex(imyblock)
  RowSize = nRow(imyblock)
  ColSize = nCol(imyblock)
 ! AddSymBlock = Dsym
 ! IF((iglobalstartrow.EQ.iglobalstartcol).AND.&
 !      (RowSize.EQ.ColSize))AddSymBlock = .FALSE.
  
!  print*,'ADD BLOCK   imyblock=',imyblock,'MYNUM',infpar%mynum
!  print*,'RowSize',RowSize,'ColSize',ColSize
!  print*,'iglobalstartrow',iglobalstartrow
!  print*,'iglobalstartcol',iglobalstartcol
!  call ls_output(ATMP(:,:,imyblock),1,RowSize,1,ColSize,nMaxRow,nMaxCol,1,6)
  
  call determineAtomsArray(RowSize,iglobalstartrow,&
       & AtomsArray1,nAtoms1,nAtomsArray1,AO1)
  
  call determineAtomsArray(ColSize,iglobalstartcol,&
       & AtomsArray2,nAtoms2,nAtomsArray2,AO2)

!  print*,'AtomsArray1(1:nAtomsArray1)',AtomsArray1(1:nAtomsArray1)
!  print*,'AtomsArray2(1:nAtomsArray2)',AtomsArray2(1:nAtomsArray2)

  IF(MAXVAL(ABS(ATMP(1:RowSize,1:ColSize,imyblock))).GT.1.0E-8_realk)THEN
   sc = 0
   DO J=1,nAtomsArray2
    JATOM = AtomsArray2(J)
    n2 = AO2%ATOMICnORB(JATOM) 
    sr = 0
    DO I=1,nAtomsArray1
     IATOM = AtomsArray1(I)
     n1 = AO1%ATOMICnORB(IATOM) 

     iLSAO = lst_DRHS%INDEX(IATOM,JATOM,1,1)
!     print*,'MAXVAL(ATMP)=',MAXVAL(ATMP(sr+1:MIN(nMaxRow,sr+nDistRow),sc+1:MIN(nMaxCol,sc+nDistCol),imyblock))
     IF(iLSAO.EQ.0)THEN !CYCLE  !BLOCK IS ZERO 
        !BLOCK IS ZERO ??
        JJ = 1
        DO II=1,IATOM-1
           JJ = JJ + AO1%ATOMICnBATCH(II) 
        ENDDO
        sA = lst_DRHS%AO(1)%p%BATCH(JJ)%startOrbital(1)
        JJ = 1
        DO II=1,JATOM-1
           JJ = JJ + AO2%ATOMICnBATCH(II) 
        ENDDO
        sB = lst_DRHS%AO(2)%p%BATCH(JJ)%startOrbital(1)
        call determinenDistRow(nDistRow,sA,iglobalstartrow,n1,RowSize,DistRowLST,DistRowATMP,nbastRHS1)
        call determinenDistRow(nDistCol,sB,iglobalstartcol,n2,ColSize,DistColLST,DistColATMP,nbastRHS2)

     ELSE
        maxBat = lst_DRHS%LSAO(iLSAO)%maxBat
        maxAng = lst_DRHS%LSAO(iLSAO)%maxAng 
        factor = 1.0E0_realk
        sA = lst_DRHS%LSAO(iLSAO)%startGlobalOrb(1)
        sB = lst_DRHS%LSAO(iLSAO)%startGlobalOrb(1+maxAng*maxBat)    
        call determinenDistRow(nDistRow,sA,iglobalstartrow,n1,RowSize,DistRowLST,DistRowATMP,nbastRHS1)
        call determinenDistRow(nDistCol,sB,iglobalstartcol,n2,ColSize,DistColLST,DistColATMP,nbastRHS2)


!        print*,'n1 =',n1,' n2 =',n2,' sA=',sA,' sB=',sB,'nbastRHS1',nbastRHS1,'nbastRHS2',nbastRHS2,' MYNUM=',infpar%mynum
!        print*,'sr+1',sr+1,' : ',MIN(nMaxRow,sr+nDistRow)
!        print*,'sc+1',sc+1,' : ',MIN(nMaxCol,sc+nDistCol)
!        WRITE(6,'(A,I5,A,I5)') 'sr = ',sr,' nDistRow',nDistRow
!                            index in LST     index in ATMP        global index 
!        WRITE(6,'(3A18)') 'DistRowLST(I)','sr+DistRowATMP(I)','sA+DistRowLST(I)-1'
!        DO II=1,nDistRow
!           WRITE(6,'(3I18)') DistRowLST(II),sr+DistRowATMP(II),sA+DistRowLST(II)-1
!        ENDDO
!        FLUSH(6)
!        WRITE(6,'(A,I5,A,I5)') 'sc = ',sc,' nDistCol=',nDistCol
!        WRITE(6,'(3A18)') 'DistColLST(I)','sc+DistColATMP(I)','sA+DistColLST(I)-1'
!        DO II=1,nDistCol
!           WRITE(6,'(3I18)') DistColLST(II),sc+DistColATMP(II),sA+DistColLST(II)-1
!        ENDDO
!        FLUSH(6)
        
        call AddLstBlock(lst_DRHS%LSAO(iLSAO)%elms,lst_DRHS%LSAO(iLSAO)%nelms,&
             & n1,n2,nDmatRHS,ATMP,nMaxRow,&
             & nMaxCol,imyblock,nLocalBlocks,nDistRow,nDistCol,DistRowLST,DistRowATMP,&
             & DistColLST,DistColATMP,sA,sB,sr,sc,IDMAT,factor,nbastRHS1,nbastRHS2)
        
     ENDIF
     sr = sr + nDistRow
    ENDDO
    sc = sc + nDistCol
   ENDDO
  ENDIF
 ENDDO
ENDDO

call lstensor_family_reorder2D(lst_DRHS)

!call lsmpi_barrier(scalapack_comm)
!call sleep(3*infpar%mynum)
! 
!print*,'LST_DRHS    MYNUM=',infpar%mynum
!call lstensor_print(lst_DRHS,6)
!
!call lsmpi_barrier(scalapack_comm)

!call lsquit('TEST DONE',-1)
call mem_dealloc(AtomsArray1)
call mem_dealloc(AtomsArray2)
call mem_dealloc(ATMP)
call mem_dealloc(Rowindex)
call mem_dealloc(Colindex)
call mem_dealloc(nRow)
call mem_dealloc(nCol)
#else
call lsquit('lsutil_scalapack_build_lst_from_matarrayB',-1)
#endif
end subroutine lsutil_scalapack_build_lst_from_matarray

subroutine determinenDistRow(nDistRow,sA,iglobalstartrow,n1,RowSize,DistRowLST,&
     & DistRowATMP,nbast)
  implicit none
  integer,intent(in) :: sA,iglobalstartrow,n1,RowSize,nbast
  integer,intent(inout) :: nDistRow,DistRowLST(nbast),DistRowATMP(nbast)
  !
  integer :: II
!  print*,'D: sA=',sA,'.GE.',iglobalstartrow,'=',sA.GE.iglobalstartrow
!  print*,'D: sA+n1-1=',sA+n1-1,'.LE.',iglobalstartrow+RowSize-1,'=',sA+n1-1.LE.iglobalstartrow+RowSize-1
  IF(sA.GE.iglobalstartrow.AND.sA+n1-1.LE.iglobalstartrow+RowSize-1)THEN
     nDistRow = n1
     DO II=1,n1
        DistRowLST(II) = II
        DistRowATMP(II) = II
     ENDDO
  ELSE
     nDistRow = 0
     DO II=1,n1
        IF(sA+II-1.GE.iglobalstartrow.AND.sA+II-1.LE.iglobalstartrow+RowSize-1)THEN
           nDistRow = nDistRow + 1
           DistRowLST(nDistRow) = II
           DistRowATMP(nDistRow) = nDistRow
        ENDIF
     ENDDO
  ENDIF
end subroutine determinenDistRow

subroutine AddLstBlock(elms,nelms,n1,n2,nDmatRHS,ATMP,nMaxRow,&
     & nMaxCol,iBlocks,nLocalBlocks,nDistRow,nDistCol,DistRowLST,DistRowATMP,&
     & DistColLST,DistColATMP,sA,sB,sr,sc,IDMAT,factor,nbast1,nbast2)
implicit none
integer,intent(in) :: nelms,nDmatRHS,nMaxRow,nMaxCol,nbast1,nbast2
integer,intent(in) :: iBlocks,nLocalBlocks,n1,n2,IDMAT,nDistCol
integer,intent(in) :: sA,sB,nDistRow,DistRowLST(nbast1),DistRowATMP(nbast1)
integer,intent(in) :: DistColLST(nbast2),DistColATMP(nbast2)
real(realk),intent(in) :: ATMP(nMaxRow,nMaxCol,nLocalBlocks),factor
real(realk),intent(inout) :: elms(n1,n2,nDmatRHS)
integer,intent(in) :: sr,sc
!
integer :: I,J
DO J=1,nDistCol
   DO I=1,nDistRow
!      print*,'I=',I
!      WRITE(6,'(A,I3,A,I3,A,I3,A,I3,A,I3,A,F16.8)')'A2elms(',sA+DistRowLST(I)-1,',',&
!           & sB+DistColLST(J)-1&
!           & ,') = ATMP(',sr+DistRowATMP(I),',',sc+DistColATMP(J),',',iBlocks,') =+ ',&
!           & ATMP(sr+DistRowATMP(I),sc+DistColATMP(J),iBlocks)

!      WRITE(6,'(A,I3,A,I3,A,I3,A,I3,A,I3,A,F16.8)')'A2elms(',DistRowLST(I),',',DistColLST(J)&
!           &,') =? ATMP(',sr+DistRowATMP(I),',',sc+DistColATMP(J),',',iBlocks,') =+ ',&
!           & ATMP(sr+DistRowATMP(I),sc+DistColATMP(J),iBlocks)
!      WRITE(6,'(A,I3,A,I3,A,I3,A,I3,A,I3,A,F16.8)')'A2elms(',DistRowLST(I),',',DistColLST(J)&
!           &,') =? ATMP(',sr+DistRowATMP(I),',',sc+DistColATMP(J),',',iBlocks,') =+ ',&
!           & ATMP(sr+DistRowATMP(I),sc+DistColATMP(J),iBlocks)
      
      elms(DistRowLST(I),DistColLST(J),IDMAT) = elms(DistRowLST(I),DistColLST(J),IDMAT) + &
           & ATMP(sr+DistRowATMP(I),sc+DistColATMP(J),iBlocks)
   ENDDO
ENDDO
end subroutine AddLstBlock

subroutine AddLstBlock2(elms,nelms,n1,n2,nDmatRHS,ATMP,nMaxRow,&
     & nMaxCol,iBlocks,nLocalBlocks,nDistRow,nDistCol,DistRowLST,DistRowATMP,&
     & DistColLST,DistColATMP,sA,sB,sr,sc,IDMAT,factor,nbast1,nbast2)
implicit none
integer,intent(in) :: nelms,nDmatRHS,nMaxRow,nMaxCol,nbast1,nbast2
integer,intent(in) :: iBlocks,nLocalBlocks,n1,n2,IDMAT,nDistCol
integer,intent(in) :: sA,sB,nDistRow,DistRowLST(nbast1),DistRowATMP(nbast1)
integer,intent(in) :: DistColLST(nbast2),DistColATMP(nbast2)
real(realk),intent(in) :: ATMP(nMaxRow,nMaxCol,nLocalBlocks),factor
real(realk),intent(inout) :: elms(n2,n1,nDmatRHS)
integer,intent(in) :: sr,sc
!
integer :: I,J
DO J=1,nDistCol
   DO I=1,nDistRow
!      WRITE(6,'(A,I3,A,I3,A,I3,A,I3,A,I3,A)')'B2elms(',sB+DistColLST(J)-1,',',sA+DistRowLST(I)-1&
!           & ,') = ATMP(',sr+DistRowATMP(I),',',sc+DistColATMP(J),',',iBlocks,')'
      elms(DistColLST(J),DistRowLST(I),IDMAT) = elms(DistColLST(J),DistRowLST(I),IDMAT) + &
           & factor*ATMP(sr+DistRowATMP(I),sc+DistColATMP(J),iBlocks)
   ENDDO
ENDDO
end subroutine AddLstBlock2

subroutine determineAtomsArray(RowSize,iglobalstartrow,&
     & AtomsArray1,nAtoms1,nAtomsArray1,AO1)
implicit none
integer,intent(in) :: nAtoms1,RowSize,iglobalstartrow
integer,intent(inout) :: AtomsArray1(nAtoms1)
integer,intent(inout) :: nAtomsArray1
TYPE(AOITEM),intent(in) :: AO1

!local variables
integer :: iOrb,IATOM,nOrb 
logical :: OutsideBLOCK
OutsideBLOCK = .TRUE.
nAtomsArray1 = 0 
iOrb = 0
rowAtomLoop1: DO IATOM = 1,nAtoms1
   nOrb = AO1%ATOMICnORB(IATOM) 
!   print*,'OutsideBLOCK',OutsideBLOCK
   IF(OutsideBLOCK)THEN
!      print*,'iOrb + nOrb=',iOrb + nOrb,'.GE.',iglobalstartrow,'=',iOrb + nOrb.GE.iglobalstartrow
      IF(iOrb + nOrb.GE.iglobalstartrow)THEN
!         print*,'INCLUDE ATOM=',IATOM,' ORB:',iOrb + 1,':',iOrb + nOrb
         OutsideBLOCK=.FALSE.
         nAtomsArray1 = nAtomsArray1 + 1
         AtomsArray1(nAtomsArray1) = IATOM
      ELSE
         !still outside
      ENDIF
   ELSE
      !inside block
      nAtomsArray1 = nAtomsArray1 + 1
      AtomsArray1(nAtomsArray1) = IATOM
!      print*,'INCLUDE ATOM=',IATOM,' ORB:',iOrb + 1,':',iOrb + nOrb
   ENDIF
!   print*,'EXIT? iOrb + nOrb=',iOrb + nOrb,'.GT.',iglobalstartrow+RowSize-1,'=',iOrb + nOrb.GT.iglobalstartrow+RowSize-1
   IF(iOrb + nOrb.GT.iglobalstartrow+RowSize-1)THEN
      !Outside Block
      EXIT rowAtomLoop1
   ENDIF
   iOrb = iOrb + nOrb
ENDDO rowAtomLoop1
end subroutine determineAtomsArray

END MODULE Lsutil_ScalapackMod



