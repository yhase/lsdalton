!> @file

!> Subroutines related to the construction of the CCSD residual
!> of the left transformation using tensor framework
!> \author Dmytro Bykov, Patrick Ettenhuber, Dmitry Lyakh 
!> \date   OCT 2015

module ccsd_left_trans

  ! ================= Declarations / Specifications =================
  use lsmpi_Bcast
  use fundamental
  use precision
  use tensor_interface_module
  use dec_typedef_module
  use typedeftype
  use screen_mod
  use iso_c_binding,only:c_ptr
  use lstiming!, only: lstimer
  use cc_tools_module
  use background_buffer_module
  use ccintegrals
  use reorder_frontend_module
  use BUILDAOBATCH
  use decmpi_module
  use dec_fragment_utils
  use IntegralInterfaceDEC
  use memory_handling

#ifdef VAR_MPI
  use infpar_module
  use lsmpi_type
  use lsmpi_module
  use lsmpi_param
  use lsmpi_Buffer
  use lsmpi_ReductionMod
#endif
  ! ========================= Implementation ========================
  public :: get_ccsd_multipliers_parallel
  private

  contains


  !> ----------------------------------------------------------------
  !> \brief Calculate contributions to the left transformation 
  !>        residual
  !> ON INPUT
  !>            t1, t2          - converged CCSD amplitudes
  !>            m1, m2          - guess for Lagrange multipliers
  !>            govov           - mo integrals
  !>            fo              - ao fock matrix
  !>            xo,yo,xv,yv     - transformation matricies 
  !>            no,nv,nb        - dimentions 
  !>            iter            - current iteration 
  !>            MyLsItem        - integral info stuff
  !> ON OUTPUT
  !>            rho1,rho2       - residuals 
  !> \author Dmytro Bykov
  !> \start date   OCT 2015
  !> ----------------------------------------------------------------
  subroutine get_ccsd_multipliers_parallel&
                                      &(rho1,rho2,&    !residuals 
                                      & govov,&        !integrals
                                      & t1,t2,m1,m2,&  !amps and mults
                                      & fo,&           !AO Fock matrix 
                                      & xo,yo,xv,yv,&  !transformation matricies
                                      & no,nv,nb,&     !number of occ, virt, basis
                                      & iter,&
                                      & MyLsItem)

    implicit none
 
    !INPUT declarations
    !******************
    type(tensor),intent(inout)    :: m1,m2
    type(tensor),intent(inout)    :: t1,t2
    type(tensor),intent(inout)    :: govov
    type(tensor),intent(inout)    :: xo,yo,xv,yv
    type(tensor),intent(inout)    :: fo
    integer,intent(in)            :: no,nv,nb
    type(lsitem),intent(inout)    :: MyLsItem
    integer,intent(in)            :: iter 

    !OUTPUT
    !******
    type(tensor),intent(inout)    :: rho1,rho2
 
    !LOCAL INTERMEDIATES
    !*******************
    type(tensor)                  :: tmp1
    type(tensor)                  :: tmp2
    type(tensor)                  :: goovv,gooov,gvooo,goooo,gvoov
    type(tensor)                  :: lovov,lvoov,lovoo
    type(tensor)                  :: t1Fov,t1Fvv,t1Foo
    type(tensor)                  :: R2
    type(tensor)                  :: X1,X2,X3
    type(tensor)                  :: X4
    type(tensor)                  :: X5,X6,X7
    type(tensor)                  :: R1
    type(tensor)                  :: gvvvv, govvv
    type(mpi_realk)               :: w1
    integer(kind=ls_mpik)         :: lg_me, mode
    character                     :: intspec(5),MOSPEC(4)
    integer(kind=ls_mpik)         :: me,nnod
 
    !Service variables
    integer                       :: i,j,a,os,vs,bs
    integer                       :: V2Batched,V4Batched
    integer                       :: blv2,blv4,bl
    integer                       :: DebugPrint=3
    logical                       :: master,local,bg,collective

    !INITIALIZE LOCAL INTERMEDIATES
    !******************************
    intspec = ['R','R','R','R','C']
    local   = .true.
    collective = .true.
    vs = tensor_get_tdim_idx(t2,1)
    os = tensor_get_tdim_idx(t2,4)
    bs = vs

#ifdef VAR_MPI
    local = infpar%lg_nodtot==1
#endif

    !GET SLAVES
    me=0
    bg=.false.
#ifdef VAR_MPI
    me   = infpar%lg_mynum
    nnod = infpar%lg_nodtot
    mode = MPI_MODE_NOCHECK
    if(.not.local.and.me == infpar%master)then
      call get_slaves_ccsd_multipliers_parallel(rho1,rho2,govov,&
                                           &t1,t2,m1,m2,&
                                           &xo,yo,xv,yv,&
                                           &fo,iter,&
                                           & no,nv,nb,&
                                           & MyLsItem)
    endif 

    bg = mem_is_background_buf_init()
#endif

    !********************************
    !Get all intermediates
    !******************************** 
    !Get X2 initialized 
    call tensor_ainit(X2, [nv,nv], local=local, Ttype=TT_TiledDistributedTensor, tdim=[vs,vs],bg=bg )
    call tensor_ainit(R1, [nv,no], local=local, Ttype=TT_TiledDistributedTensor, tdim=[vs,os],bg=bg )
    call tensor_zero(R1)
    call tensor_contract(X2,'X2(fe)=z2(dkfl)t2(dkel)',m2,t2)
    if(DECinfo%PL>DebugPrint)then
      call print_norm(X2,"X2 norm:",print_=.true.)
    endif
    
    !Get needed Fock intermediates
    call tensor_ainit(tmp1,  [nb,no], Ttype=TT_DenseTensor)
    call tensor_ainit(tmp2,  [nb,nv], Ttype=TT_DenseTensor)
    call tensor_ainit(t1Foo, [no,no], Ttype=TT_DenseTensor)
    call tensor_ainit(t1Fov, [no,nv], Ttype=TT_DenseTensor)
    call tensor_ainit(t1Fvv, [nv,nv], Ttype=TT_DenseTensor)
    call tensor_contract(tmp1,'T(alpha,i) = F(alpha,beta)*X(beta,i)',fo,yo)
    call tensor_contract(t1Foo,'F(i,j) = X(alpha,i)*T(alpha,j)',xo,tmp1)
    call tensor_contract(tmp2,'T(alpha,a) = F(alpha,beta)*X(beta,a)',fo,yv)
    call tensor_contract(t1Fov,'F(i,a) = X(alpha,i)*T(alpha,a)',xo,tmp2)
    call tensor_contract(t1Fvv,'F(a,b) = X(alpha,a)*T(alpha,b)',xv,tmp2)
    call tensor_free(tmp2)
    call tensor_free(tmp1)

    if(DECinfo%PL>DebugPrint)then
      call print_norm(t1Fov,"t1Fov norm:",print_=.true.)
      call print_norm(t1Fvv,"t1Fvv norm:",print_=.true.)
      call print_norm(t1Foo,"t1Foo norm:",print_=.true.)
    endif

    !Get classes of MO integrals
    call tensor_ainit(goovv, [no,no,nv,nv], local=local, Ttype=TT_TiledDistributedTensor, tdim=[os,os,vs,vs],bg=bg )
    call tensor_zero(goovv)
    IF(DECinfo%RICCSDINTEGRALS)THEN
       MOSPEC(1) = 'O';  MOSPEC(2) = 'Y';  MOSPEC(3) = 'X';  MOSPEC(4) = 'V'     
       call get_mo_riintegral_par(goovv,xo,yo,xv,yv,mylsitem,intspec,local,MOSPEC)
    ELSE
       call get_mo_integral_par(goovv,xo,yo,xv,yv,mylsitem,intspec,local)
    ENDIF
    call tensor_ainit(gooov, [no,no,no,nv], local=local, Ttype=TT_TiledDistributedTensor, tdim=[os,os,os,vs],bg=bg )
    IF(DECinfo%RICCSDINTEGRALS)THEN
       MOSPEC(1) = 'O';  MOSPEC(2) = 'Y';  MOSPEC(3) = 'O';  MOSPEC(4) = 'V'     
       call get_mo_riintegral_par(gooov,xo,yo,xo,yv,mylsitem,intspec,local,MOSPEC)
    ELSE
       call get_mo_integral_par(gooov,xo,yo,xo,yv,mylsitem,intspec,local)
    ENDIF
    call tensor_ainit(gvooo, [nv,no,no,no], local=local, Ttype=TT_TiledDistributedTensor, tdim=[vs,os,os,os],bg=bg )
    IF(DECinfo%RICCSDINTEGRALS)THEN
       MOSPEC(1) = 'X';  MOSPEC(2) = 'Y';  MOSPEC(3) = 'O';  MOSPEC(4) = 'Y'     
       call get_mo_riintegral_par(gvooo,xv,yo,xo,yo,mylsitem,intspec,local,MOSPEC)
    ELSE
       call get_mo_integral_par(gvooo,xv,yo,xo,yo,mylsitem,intspec,local)
    ENDIF
    call tensor_ainit(goooo, [no,no,no,no], local=local, Ttype=TT_TiledDistributedTensor, tdim=[os,os,os,os],bg=bg )
    IF(DECinfo%RICCSDINTEGRALS)THEN
       MOSPEC(1) = 'O';  MOSPEC(2) = 'Y';  MOSPEC(3) = 'O';  MOSPEC(4) = 'Y'     
       call get_mo_riintegral_par(goooo,xo,yo,xo,yo,mylsitem,intspec,local,MOSPEC)
    ELSE
       call get_mo_integral_par(goooo,xo,yo,xo,yo,mylsitem,intspec,local)
    ENDIF
       
    if(DECinfo%PL>DebugPrint)then
      call print_norm(goovv,"goovv norm:",print_=.true.)
      call print_norm(gooov,"gooov norm:",print_=.true.)
      call print_norm(gvooo,"gvooo norm:",print_=.true.)
      call print_norm(goooo,"goooo norm:",print_=.true.)
    endif
    
    !Get classes of L
    call tensor_ainit(lovov, [no,nv,no,nv], local=local, Ttype=TT_TiledDistributedTensor, tdim=[os,vs,os,vs],bg=bg )
    call tensor_add(lovov, 2.0E0_realk,govov, a = 0.0E0_realk)
    call tensor_add(lovov,-1.0E0_realk,govov, order=[1,4,3,2] )

    call tensor_ainit(lvoov, [nv,no,no,nv], local=local, Ttype=TT_TiledDistributedTensor, tdim=[vs,os,os,vs],bg=bg )
    call tensor_ainit(gvoov, [nv,no,no,nv], local=local, Ttype=TT_TiledDistributedTensor, tdim=[vs,os,os,vs],bg=bg )
    IF(DECinfo%RICCSDINTEGRALS)THEN
       MOSPEC(1) = 'X';  MOSPEC(2) = 'Y';  MOSPEC(3) = 'O';  MOSPEC(4) = 'V'     
       call get_mo_riintegral_par(gvoov,xv,yo,xo,yv,mylsitem,intspec,local,MOSPEC)
    ELSE
       call get_mo_integral_par(gvoov,xv,yo,xo,yv,mylsitem,intspec,local)
    ENDIF
    call tensor_add(lvoov, 2.0E0_realk,gvoov, a = 0.0E0_realk)
    call tensor_free(gvoov)
    call tensor_add(lvoov,-1.0E0_realk,goovv, order=[3,2,1,4] )

    call tensor_ainit(lovoo, [no,nv,no,no], local=local, Ttype=TT_TiledDistributedTensor, tdim=[os,vs,os,os],bg=bg )
    call tensor_add(lovoo, 2.0E0_realk,gooov, order=[3,4,1,2], a = 0.0E0_realk)
    call tensor_add(lovoo,-1.0E0_realk,gooov, order=[1,4,3,2] )

    if(DECinfo%PL>DebugPrint)then
      call print_norm(lovov,"lovov norm:",print_=.true.)
      call print_norm(lovoo,"lovoo norm:",print_=.true.)
      call print_norm(lvoov,"lvoov norm:",print_=.true.)
    endif

    !Get X2 initialized 
    call tensor_ainit(X5, [nv,no,no,no], local=local, Ttype=TT_TiledDistributedTensor, tdim=[vs,os,os,os],bg=bg )

    !********************************
    !Do the contructions in batches
    !******************************** 

    !Here loop over two batched virtual indices must be started
    !DEBUG - so far:  
    bl   = nv

    !start loop over 2 batced MO virtual indecies 
    do V2Batched = 1, nv, bl
      do V4Batched = 1, nv, bl

        !Calculate 4 batched MO integrals
        !******************************** 
             
        if(nv-V2Batched<bl)then 
          blv2 = nv-V2Batched+1
        else
          blv2 = bl
        endif 

        if(nv-V4Batched<bl)then 
          blv4 = nv-V4Batched+1
        else
          blv4 = bl
        endif 

        !Init the tensors - gvvvv, govvv and X4
        call tensor_ainit( govvv, [no,blv2,nv,blv4], local=local, Ttype=TT_TiledDistributedTensor, tdim=[os,vs,vs,vs],bg=bg ) 
        call tensor_ainit( X4, [nv,no,nv,blv4], local=local, Ttype=TT_TiledDistributedTensor, tdim=[vs,os,vs,vs],bg=bg ) 
        call tensor_ainit( gvvvv, [nv,blv2,nv,blv4], local=local, Ttype=TT_TiledDistributedTensor, tdim=[vs,vs,vs,vs],bg=bg ) 
        !call tensor_ainit( X4v2, [nv,no,nv,blv2], local=local, Ttype=TT_TiledDistributedTensor, tdim=[vs,os,vs,vs] ) 
        !call tensor_ainit( X4v4, [nv,no,nv,blv4], local=local, Ttype=TT_TiledDistributedTensor, tdim=[vs,os,vs,vs] ) 

        !Get the gvvvv, govvv
        call get_4_batched_mo_int_par(gvvvv,govvv,xo,xv,yo,yv,X4,V2Batched,&
                                      &V4Batched,blv2,blv4,MyLsItem,intspec,local,os,vs,bs)

        if(DECinfo%PL>DebugPrint)then
          call print_norm(m2,   "m2:",   print_=.true.)
          call print_norm(gvvvv,"gvvvv:",print_=.true.)
          call print_norm(govvv,"govvv:",print_=.true.)
          call print_norm(X4,   "X4:",   print_=.true.)
        endif

        !Calculate the intermediates
        !******************************** 
        call tensor_contract(rho2,'R2(aibj)+=alpha*m2(cidj)g(cadb)',m2,gvvvv,alpha=0.5E0_realk,force_sync=.true.)
        !destroy integral gcadb now!
        call tensor_free(gvvvv)
        if(DECinfo%PL>DebugPrint)then
          call print_norm(rho2,"R2 1:",print_=.true.)
        endif
       
        !X4 intermediate 
        call tensor_contract(X4,'X4(dkea)+=t2(djfk)g(jfea)*alpha',t2,govvv,alpha=-1.0E0_realk,force_sync=.true.)
        call tensor_contract(X4,'X4(dkea)+=t2(ejfk)g(jadf)*alpha',t2,govvv,alpha=-1.0E0_realk,force_sync=.true.)
        call tensor_contract(X4,'X4(dkea)+=t2(dkfj)g(jfea)*alpha',t2,govvv,alpha= 2.0E0_realk,force_sync=.true.) 
        call tensor_contract(X4,'X4(dkea)+=t2(dkfj)g(jaef)*alpha',t2,govvv,alpha=-1.0E0_realk,force_sync=.true.) 
        if(DECinfo%PL>DebugPrint)then
          call print_norm(t2,"t2 check:",print_=.true.)
          call print_norm(X4,"X4 check:",print_=.true.)
        endif

        !R2 intermediate 
        !DEBUG - syncronize on the spot!
#ifdef VAR_MPI
        call tensor_sync_replicated(m1)
#endif
        call tensor_contract(rho2,'R2(aibj)+=m1(cj)g(iacb)*alpha',m1,govvv,alpha= 2.0E0_realk,force_sync=.true.)
        call tensor_contract(rho2,'R2(aibj)+=m1(cj)g(ibca)*alpha',m1,govvv,alpha=-1.0E0_realk,force_sync=.true.)
        if(DECinfo%PL>DebugPrint)then
          call print_norm(rho2,"R2 2:",print_=(me==0))
        endif

        !R1 intermediate
        call tensor_contract(R1,'R1(ai)+=X2(cb)g(iacb)*alpha',X2,govvv,alpha= 2.0E0_realk)
        call tensor_contract(R1,'R1(ai)+=X2(cb)g(ibca)*alpha',X2,govvv,alpha=-1.0E0_realk)
        call tensor_contract(R1,'R1(ai)+=m2(dkei)X4(dkea)',m2,X4,force_sync=.true.) 
        if(DECinfo%PL>DebugPrint)then
          call print_norm(R1,"R1 X2*giacb:",print_=.true.)
        endif

        !TODO this must be handled properly
        !if (v4!=v2) 
        !call tensor_contract(rho1,'R1(ai)+=m2(dkei)X4(dkea)',m2,X4,force_sync=.true.) 
        !call print_norm(rho1,"rho1:",print_=.true.)
        !destroy X4(dkea)
        call tensor_free(X4)

        !X5 intermediate
        call tensor_contract(X5,'X5(dkil)=t2(fkel)g(iedf)*alpha',t2,govvv,alpha=-1.0E0_realk,force_sync=.true.) 
        if(DECinfo%PL>DebugPrint)then
          call print_norm(X5,"X5:",print_=.true.)
        endif
        !destroy g(jfea) 
        call tensor_free(govvv)

      enddo!V2Batched = 1, nv, bl  
    enddo!V4Batched = 1, nv, bl
    
    call tensor_contract(R1,'R1(ai)+=F(ie)X2(ae)*alpha',t1Fov,X2,alpha=-1.0E0_realk,force_sync=.true.)
    if(DECinfo%PL>DebugPrint)then
      call print_norm(R1,"R1 F*X2:",print_=.true.)
    endif

    !o3v3 contractions:
    !******************
    call tensor_ainit(X1,[nv,no,no,nv],local=local,Ttype=TT_TiledDistributedTensor,tdim=[vs,os,os,vs],bg=bg )
    !L:o2v2(ovov)
    call tensor_contract(X1,'X1(bkia)= t2(bkdl)L(ldia)*alpha',t2,lovov,alpha=2.0E0_realk,force_sync=.true.)    
    call tensor_contract(X1,'X1(bkia)+=t2(bldk)L(ldia)*alpha',t2,lovov,alpha=-1.0E0_realk,force_sync=.true.)
    !L:o2v2(voov)
    call tensor_add(X1,1.0E0_realk,lvoov)
    call tensor_contract(R1,'R1(ai)+=X1(bkia)z1(bk)',X1,m1,force_sync=.true.)

    if(DECinfo%PL>DebugPrint)then
      call print_norm(R1,"R1 X1*z1:",print_=.true.)
    endif

    call tensor_contract(X1,'X1(eimb)=t2(enfm)g(nbif)*alpha',t2,govov,alpha=-1.0E0_realk,force_sync=.true.)
    call tensor_add(X1,1.0E0_realk,goovv,order=[2,3,1,4])
    call tensor_contract(rho2,'R2(aibj)+=z2(amej)X1(eimb)*alpha',m2,X1,alpha=-1.0E0_realk,force_sync=.true.)
    call tensor_contract(rho2,'R2(aibj)+=z2(ajem)X1(eimb)*alpha',m2,X1,alpha=-0.5E0_realk,force_sync=.true.)
 
    call tensor_contract(X1,'X1(emjb) =t2(emfn)L(nfjb)*alpha',t2,lovov,alpha=2.0E0_realk,force_sync=.true.)
    call tensor_contract(X1,'X1(emjb)+=t2(enfm)L(nfjb)*alpha',t2,lovov,alpha=-1.0E0_realk,force_sync=.true.)
    call tensor_add(X1,1.0E0_realk,lvoov)
    call tensor_contract(rho2,'R2(aibj)+=z2(aiem)X1(emjb)',m2,X1,force_sync=.true.)
    call tensor_contract(rho2,'R2(aibj)+=z2(ajem)X1(emib)*alpha',m2,X1,alpha=-0.5E0_realk,force_sync=.true.)
    call tensor_free(X1)

    if(DECinfo%PL>DebugPrint)then
      call print_norm(rho2,"rho2 z2*X1:",print_=.true.)
    endif

    !# o4v2 contractions:
    !********************
    call tensor_contract(X5,'X5(dkil)+=t2(dkfj)L(jfil)*alpha',t2,lovoo,alpha=-1.0E0_realk,force_sync=.true.)
    call tensor_contract(X5,'X5(dkil)+=t2(djfl)g(jkif)',t2,gooov,force_sync=.true.)
    call tensor_contract(X5,'X5(dkil)+=t2(djfk)g(iljf)',t2,gooov,force_sync=.true.)
    call tensor_add(X5,-1.0E0_realk,gvooo)
    call tensor_contract(R1,'R1(ai)+=z2(dkal)X5(dkil)',m2,X5,force_sync=.true.)
    call tensor_free(X5)

    call tensor_ainit(X6,[no,no,no,no],local=local,Ttype=TT_TiledDistributedTensor,tdim=[os,os,os,os],bg=bg )
    call tensor_contract(X6,'X6(kijl)=z2(dkei)t2(djel)',m2,t2,force_sync=.true.)
    call tensor_contract(R1,'R1(ai)+=X6(kijl)g(jkla)',X6,gooov,force_sync=.true.)

    call tensor_contract(X6,'X6(jnim)=t2(fnem)g(jfie)',t2,govov,force_sync=.true.)
    call tensor_add(X6,1.0E0_realk,goooo)
    call tensor_contract(rho2,'R2(aibj)+=z2(ambn)X6(jnim)*alpha',m2,X6,alpha=0.5E0_realk,force_sync=.true.)

    call tensor_contract(X6,'X6(ijmn)=z2(cidj)t2(cmdn)',m2,t2,force_sync=.true.)
    call tensor_contract(rho2,'R2(aibj)+=X6(ijmn)g(manb)*alpha',X6,govov,alpha=0.5E0_realk,force_sync=.true.)
    call tensor_free(X6)

    if(DECinfo%PL>DebugPrint)then
      call print_norm(R1,"rho1 z2*X5:",print_=.true.)
      call print_norm(rho2,"rho2 X6*g:",print_=.true.)
    endif

    !# 5th power contractions:
    !*************************
    !#L:o2v2(ovov)
    call tensor_cp_data(t1Fvv,X2)
    call tensor_contract(X2,'X2(ba)+=t2(dlbk)L(ldka)*alpha',t2,lovov,alpha=-1.0E0_realk,force_sync=.true.)        
    call tensor_contract(R1,'R1(ai)+=z1(bi)X2(ba)',m1,X2,force_sync=.true.)

    call tensor_contract(X2,'X2(af)=z2(eman)t2(emfn)',m2,t2,force_sync=.true.)
    call tensor_contract(rho2,'R2(aibj)+=L(ifjb)X2(af)*alpha',lovov,X2,alpha=-1.0E0_realk,force_sync=.true.)

    call tensor_cp_data(t1Fvv,X2)
    call tensor_contract(X2,'X2(eb)+=t2(fnem)L(nfmb)*alpha',t2,lovov,alpha=-1.0E0_realk,force_sync=.true.)
    call tensor_contract(rho2,'R2(aibj)+=z2(aiej)X2(eb)',m2,X2,force_sync=.true.)

    !#L:o2v2(ovov)
    call tensor_ainit(X3,[no,no],local=local,Ttype=TT_TiledDistributedTensor,tdim=[os,os],bg=bg )
    call tensor_cp_data(t1Foo,X3)
    call tensor_contract(X3,'X3(ij)+=t2(djbk)L(kbid)',t2,lovov,force_sync=.true.)            
    call tensor_contract(R1,'R1(ai)+=z1(aj)X3(ij)*alpha',m1,X3,alpha=-1.0E0_realk,force_sync=.true.)

    call tensor_contract(X3,'X3(jl)=z2(dkej)t2(dkel)',m2,t2,force_sync=.true.)

    !#L:o3v1(ooov)
    call tensor_contract(R1,'R1(ai)+=L(ialj)X3(jl)*alpha',lovoo,X3,alpha=-1.0E0_realk,force_sync=.true.)           
    call tensor_contract(R1,'R1(ai)+=F(la)X3(il)*alpha',t1Fov,X3,alpha=-1.0E0_realk,force_sync=.true.)

    call tensor_contract(X3,'X3(jn)=z2(emfj)t2(emfn)',m2,t2,force_sync=.true.)
    call tensor_contract(rho2,'R2(aibj)+=L(ianb)X3(jn)*alpha',lovov,X3,alpha=-1.0E0_realk,force_sync=.true.)

    call tensor_cp_data(t1Foo,X3)
    call tensor_contract(X3,'X3(jn)+=t2(fnem)L(mejf)',t2,lovov,force_sync=.true.)
    call tensor_contract(rho2,'R2(aibj)+=z2(aibn)X3(jn)*alpha',m2,X3,alpha=-1.0E0_realk,force_sync=.true.)
    call tensor_free(X3)

    !call tensor_ainit(X3,[nv,no],local=local,Ttype=TT_TiledDistributedTensor,tdim=[vs,os])
    !call tensor_cp_data(m1,X3)
    !print *,"compare norms m1, x3",tensor_norm(m1),tensor_norm(X3)
    !call tensor_contract(rho2,'R2(aibj)+=alpha*L(jbik)*z1(ak)',lovoo,X3,alpha=-1.0E0_realk,force_sync=.true.)
    call tensor_contract(rho2,'R2(aibj)+=alpha*L(jbik)*z1(ak)',lovoo,m1,alpha=-1.0E0_realk,force_sync=.true.)
    !call tensor_contract(-1.0E0_realk,lovoo,m1,[4],[2],1,1.0E0_realk,rho2,order=[4,3,2,1],force_sync=.true.)
    !call tensor_free(X3)

    if(DECinfo%PL>DebugPrint)then
      call print_norm(R1,"rho1 F*X3:",print_=.true.)
      call print_norm(rho2,"rho2 z1*L:",print_=.true.)
    endif

    !# 4th power contractions:
    !*************************
    call tensor_contract(rho2,'R2(aibj)+=z1(ai)F(jb)*alpha',m1,t1Fov,alpha=2.0E0_realk,force_sync=.false.) 
    call tensor_contract(rho2,'R2(aibj)+=z1(aj)F(ib)*alpha',m1,t1Fov,alpha=-1.0E0_realk,force_sync=.true.)
    
    !PERMUTE
    !*********************
    call tensor_ainit(X7,[nv,no,nv,no],local=local,Ttype=TT_TiledDistributedTensor,tdim=[vs,os,vs,os],bg=bg )
    call tensor_cp_data(rho2, X7)
    call tensor_add(rho2,1.0E0_realk,X7,order=[3,4,1,2])
    call tensor_free(X7)

    !ADD RIGHT HAND SIDES 
    !********************
    call tensor_add(rho1,1.0E0_realk,R1)
    !call tensor_add(rho2,2.0E0_realk,Lovov,order=[2,1,4,3])
    !call tensor_add(rho1,2.0E0_realk,t1Fov,order=[2,1])
    if(DECinfo%PL>DebugPrint)then
      call print_norm(rho1,"R1+RHS",print_=.true.)
      call print_norm(rho2,"R2+RHS",print_=.true.)
    endif
    
    !Deallocations 
    !********************
    call tensor_free(t1Foo)
    call tensor_free(t1Fov)
    call tensor_free(t1Fvv)

    call tensor_free(lovoo)
    call tensor_free(lvoov)
    call tensor_free(lovov)
    call tensor_free(goooo)
    call tensor_free(gvooo)
    call tensor_free(gooov)
    call tensor_free(goovv)
    call tensor_free(R1)
    call tensor_free(X2)

#ifdef VAR_MPI
if(.not.local)then
    call tensor_switch_access(rho1)
    call tensor_switch_access(rho2)
    call tensor_switch_access(govov)
    call tensor_switch_access(t1)
    call tensor_switch_access(t2)
    call tensor_switch_access(m1)
    call tensor_switch_access(m2)
    call tensor_switch_access(xo)
    call tensor_switch_access(yo)
    call tensor_switch_access(xv)
    call tensor_switch_access(yv)
    call tensor_switch_access(fo)
endif
#endif

  end subroutine get_ccsd_multipliers_parallel
 
  !> ----------------------------------------------------------------
  !> \brief Construct the 4 index batched MO integrals 
  !> ON INPUT
  !>            trafo1-trafo4   - transformation matricies
  !>            mylsitem        - integral specs
  !>            m1, m2          - 
  !>            m1, m2          - 
  !> ON OUTPUT
  !>            integral        - 4-index-batched integrals 
  !> \author Dmytro Bykov and Patrick Ettenhuber 
  !> \start date   JAN 2016
  !> ----------------------------------------------------------------
  subroutine get_4_batched_mo_int_par(gvvvv,govvv,trafo1,trafo2,trafo3,trafo4,X4,V2Batched,&
                                      &V4Batched,blv2,blv4,mylsitem,INTSPEC,local,os,vs,bs)

    implicit none

    !INPUT declarations
    !******************
    type(tensor), intent(in)     :: trafo1,trafo2,trafo3,trafo4
    type(lsitem), intent(inout)  :: mylsitem
    character,    intent(inout)  :: INTSPEC(5)
    logical,      intent(in)     :: local
    integer                      :: V2Batched,V4Batched
    integer                      :: blv2,blv4
    integer, intent(in)          :: os,vs,bs
    
    !OUTPUT
    !******
    type(tensor), intent(inout)  :: gvvvv
    type(tensor), intent(inout)  :: govvv
    type(tensor), intent(inout)  :: X4

    !LOCAL INTERMEDIATES
    !*******************
    !Integral stuff
    type(tensor)   :: xo,xv,yo,yv
    logical :: save_cs_screen, save_ps_screen
    integer :: MinAObatch
    integer :: nb,no,nv,n1,n2,n3,n4
    integer :: DebugPrint=3
    integer :: i
    logical :: doscreen
    type(DecAObatchinfo),pointer :: AOGammabatchinfo(:)
    type(DecAObatchinfo),pointer :: AOAlphabatchinfo(:)
    integer :: iAO,nAObatches,iprint
    logical :: MoTrans, NoSymmetry,SameMol
    type(batchtoorb), pointer :: batch2orbAlpha(:)
    type(batchtoorb), pointer :: batch2orbGamma(:)
    integer, pointer :: orb2batchAlpha(:), batchsizeAlpha(:), batchindexAlpha(:)
    integer, pointer :: orb2batchGamma(:), batchsizeGamma(:), batchindexGamma(:)
    real(realk), pointer :: w1(:),w2(:)
    real(realk) :: MemFree
    integer(kind=long) :: maxsize
    logical :: master
    integer(kind=ls_mpik) :: me, nnod
    integer(kind=long) :: w1size, w2size
    logical :: use_bg_buf
    integer :: nbuffs
    type(tensor) :: Cint, int1, int2 
    integer :: ndimA,  ndimB,  ndimC,  ndimD
    integer :: lt
    integer :: ndimAs, ndimBs, ndimCs, ndimDs
    integer :: startA, startB, startC, startD
    integer :: mtidx(4)
    integer(kind=8) :: nbu
#ifdef VAR_MPI
    integer(kind=ls_mpik), parameter :: mode = MPI_MODE_NOCHECK
#endif
    integer :: bl,bla,blb,blg,bld
    integer :: AlphaBatched, BetaBatched
    integer :: GammaBatched, DeltaBatched
    integer :: BatchdimAlpha, BatchdimBeta, BatchdimGamma, BatchdimDelta
    real(realk), pointer :: ltp(:)

    !INITIALIZATIONS
    !***************
    master        = .true.
    me            = 0
    nnod          = 1
#ifdef VAR_MPI
    master        = (infpar%lg_mynum == infpar%master)
    me            = infpar%lg_mynum
    nnod          = infpar%lg_nodtot
#endif

    if(.not.local)then
       nbuffs         = tensor_get_ntpm_idx(gvvvv,4)
    else
       nbuffs         = 2
    endif
       
    !DEBUG DEBUG DEBUG
    !so far just without any batching
    !BUT the batching must be introduced for the second and fourth positions
    !and subsequently nv must come from outside!!!
    nb = tensor_get_dims_idx(trafo1,1)
    no = tensor_get_dims_idx(trafo1,2)
    nv = tensor_get_dims_idx(trafo2,2)
    !DEBUG DEBUG DEBUG

    doscreen = MyLsItem%setting%scheme%cs_screen.OR.MyLsItem%setting%scheme%ps_screen
    nullify(orb2batchAlpha)
    !nullify(batchdimAlpha)
    nullify(batchsizeAlpha)
    nullify(batch2orbAlpha)
    nullify(batchindexAlpha)
    nullify(orb2batchGamma)
    !nullify(batchdimGamma)
    nullify(batchsizeGamma)
    nullify(batch2orbGamma)
    nullify(batchindexGamma)

    !==================================================
    !                  Batch construction             !
    !==================================================

    use_bg_buf = .false.
#ifdef VAR_MPI
    use_bg_buf = mem_is_background_buf_init()
#endif
    if(use_bg_buf)then
       nbu = mem_get_bg_buf_free()
    else
       call get_currently_available_memory(MemFree)
       nbu = MemFree*1024**3/8
    endif

#ifdef VAR_MPI
    call lsmpi_reduce_min(nbu,infpar%master,infpar%lg_comm)
#endif
    
    ! Get free memory and determine maximum batch sizes
    ! -------------------------------------------------
    if(master)then
       call determine_maxBatchOrbitalsize(DECinfo%output,MyLsItem%setting,MinAObatch,'R')
    endif

    if(.not.local)then
#ifdef VAR_MPI
       call ls_mpiInitBuffer(infpar%master,LSMPIBROADCAST,infpar%lg_comm)
       call ls_mpi_buffer(nbuffs,infpar%master)
       call ls_mpi_buffer(INTSPEC,5,infpar%master)
       call ls_mpiFinalizeBuffer(infpar%master,LSMPIBROADCAST,infpar%lg_comm)
#endif
    endif

    !DEBUG DEBUG DEBUG
    save_cs_screen = mylsitem%setting%SCHEME%CS_SCREEN
    save_ps_screen = mylsitem%setting%SCHEME%PS_SCREEN
    mylsitem%setting%SCHEME%CS_SCREEN = .FALSE.
    mylsitem%setting%SCHEME%PS_SCREEN = .FALSE.
    doscreen = mylsitem%setting%SCHEME%CS_SCREEN.OR.mylsitem%setting%SCHEME%PS_SCREEN

    w1size = nb**2

    maxsize = w1size
    
    if( use_bg_buf ) then
       if(maxsize > nbu) then
          print *, "Warning(get_lhtr_test):  This should not happen, if the memory counting is correct&
             &, Node:",me," requests ",maxsize," in buffer ",nbu
          !call mem_change_background_alloc(maxsize*8_long)
       endif

       call mem_pseudo_alloc( w1, w1size )
    else
       call mem_alloc( w1, w1size )
    endif

    !First touch
    w1(1) = 0.0E0_realk
    
    !DEBUG DEBUG DEBUG
    !only one go through the integral loop 
    !must be changed accordingly to avail memory
    bl = nb/2
    !bl = nb
    !DEBUG DEBUG DEBUG

    call tensor_zero(gvvvv)
    call tensor_zero(govvv)
    call tensor_zero(X4)

    do AlphaBatched = 1, nb, bl
       do BetaBatched = 1, nb, bl

       !call time_start_phase(PHASE_WORK, twall = time_int1 )

       do GammaBatched = 1, nb, bl
          do DeltaBatched = 1, nb, bl

             if(nb-AlphaBatched<bl)then 
               bla = nb-AlphaBatched+1
             else
               bla = bl
             endif 
             if(nb-BetaBatched<bl)then 
               blb = nb-BetaBatched+1
             else
               blb = bl
             endif 
             if(nb-GammaBatched<bl)then 
               blg = nb-GammaBatched+1
             else
               blg = bl
             endif 
             if(nb-DeltaBatched<bl)then 
               bld = nb-DeltaBatched+1
             else
               bld = bl
             endif 

             BatchDimAlpha  = bla
             BatchDimBeta   = blb
             BatchDimGamma  = blg
             BatchDimDelta  = bld
    
             call tensor_ainit( Cint, [bla,blb,blg,bld], local=local, Ttype=TT_TiledDistributedTensor, &
                & tdim=[bs,bs,bs,bs],bg=use_bg_buf )
             call tensor_zero(Cint)

             if(tensor_is_Dense(Cint))then
                ndimA = bla
                ndimB = blb
                ndimC = blg
                ndimD = bld

                startA = AlphaBatched
                startB = BetaBatched
                startC = GammaBatched
                startD = DeltaBatched
    
                call II_GET_ERI_INTEGRALBLOCK_INQUIRE(DECinfo%output,DECinfo%output,Mylsitem%setting,&
                   & startA,startB,startC,startD,ndimA,ndimB,ndimC,ndimD,&
                   & ndimAs,ndimBs,ndimCs,ndimDs,INTSPEC)

               !allocate scratch for integrals
               w2size = ndimAs*ndimBs*ndimCs*ndimDs

               if( use_bg_buf ) then
                  call mem_pseudo_alloc( w2, w2size )
               else
                  call mem_alloc( w2, w2size )
               endif

               !First touch
               w2(1) = 0.0E0_realk

                call II_GET_ERI_INTEGRALBLOCK(DECinfo%output,DECinfo%output,Mylsitem%setting,&
                     & startA,startB,startC,startD,ndimA,ndimB,ndimC,ndimD,&
                     & ndimAs,ndimBs,ndimCs,ndimDs,INTSPEC,Cint%p%p,w2,&
                     & DECinfo%IntegralThreshold)

               !DEallocate scratch for integrals
               if( use_bg_buf )then
                  call mem_pseudo_dealloc( w2 )
               else
                  call mem_dealloc( w2 )
               endif

             else
                 do lt=1,tensor_get_nlti(Cint)
                    call get_midx(tensor_local_tile_gt(Cint,lt),mtidx,tensor_get_ntpm(Cint),tensor_get_mode(Cint))
                    
                    startA = AlphaBatched + (mtidx(1)-1)*tensor_get_tdim_idx(Cint,1)
                    startB = BetaBatched  + (mtidx(2)-1)*tensor_get_tdim_idx(Cint,2)
                    startC = GammaBatched + (mtidx(3)-1)*tensor_get_tdim_idx(Cint,3)
                    startD = DeltaBatched + (mtidx(4)-1)*tensor_get_tdim_idx(Cint,4)

                    ndimA = tensor_local_tile_dims_idx(Cint,lt,1)
                    ndimB = tensor_local_tile_dims_idx(Cint,lt,2)
                    ndimC = tensor_local_tile_dims_idx(Cint,lt,3)
                    ndimD = tensor_local_tile_dims_idx(Cint,lt,4)

                    ltp => tensor_local_tile_ptr(Cint,lt)

                    call II_GET_ERI_INTEGRALBLOCK_INQUIRE(DECinfo%output,DECinfo%output,Mylsitem%setting,&
                       & startA,startB,startC,startD,ndimA,ndimB,ndimC,ndimD,&
                       & ndimAs,ndimBs,ndimCs,ndimDs,INTSPEC)

                    !allocate scratch for integrals
                    w2size = ndimAs*ndimBs*ndimCs*ndimDs

                    if( use_bg_buf ) then
                       call mem_pseudo_alloc( w2, w2size )
                    else
                       call mem_alloc( w2, w2size )
                    endif

                    !First touch
                    w2(1) = 0.0E0_realk

                    call II_GET_ERI_INTEGRALBLOCK(DECinfo%output,DECinfo%output,Mylsitem%setting,&
                         & startA,startB,startC,startD,ndimA,ndimB,ndimC,ndimD,&
                         & ndimAs,ndimBs,ndimCs,ndimDs,INTSPEC,ltp,w2,&
                         & DECinfo%IntegralThreshold)

                    !DEallocate scratch for integrals
                    if( use_bg_buf )then
                       call mem_pseudo_dealloc( w2 )
                    else
                       call mem_dealloc( w2 )
                    endif

                 enddo
             endif

#ifdef VAR_MPI
             call tensor_barrier(.false.) 
#endif
             if(DECinfo%PL>DebugPrint)then
               call print_norm(Cint,"Test norm Cint:",print_=.true.)
             endif

             call tensor_ainit(yv, [bld,blv4], Ttype=TT_DenseTensor)
             call tensor_ainit( int2, [bla,blb,nv,blv4], local=local, Ttype=TT_TiledDistributedTensor, &
                & tdim=[bs,bs,vs,vs],bg=use_bg_buf ) 
             call tensor_ainit( int1, [bla,blb,blg,blv4],local=local, Ttype=TT_TiledDistributedTensor, &
                & tdim=[bs,bs,bs,vs],bg=use_bg_buf) 
             call copy_block_from_full_matrix(trafo4%p%p,w1,DeltaBatched,bld,V4Batched,blv4,nb,nv)
             call tensor_convert(w1,yv)
             call tensor_contract(int1,'int1(ABGb)=Cint(ABGD)trafo4(Db)',Cint,yv,force_sync=.true.)
             call tensor_free(yv)
    
             call tensor_ainit(xv, [blg,nv], Ttype=TT_DenseTensor)
             call copy_stripe_from_full_matrix(trafo2%p%p,w1,GammaBatched,blg,nb,nv)
             call tensor_convert(w1,xv)

             call tensor_contract(int2,'int2(mndb)=int1(mnrb)trafo2(rd)',int1,xv,force_sync=.true.)
             call tensor_free(int1)
    
             call tensor_ainit(yo, [blb,no], Ttype=TT_DenseTensor)
             call copy_stripe_from_full_matrix(trafo3%p%p,w1,BetaBatched,blb,nb,no)
             call tensor_convert(w1,yo)

             call tensor_free(xv)
             call tensor_ainit(xv, [bla,nv], Ttype=TT_DenseTensor)
             call copy_stripe_from_full_matrix(trafo2%p%p,w1,AlphaBatched,bla,nb,nv)
             call tensor_convert(w1,xv)

             call tensor_ainit( int1, [bla,no,nv,blv4], local=local, Ttype=TT_TiledDistributedTensor, &
                & tdim=[bs,os,vs,vs],bg=use_bg_buf ) 
             call tensor_contract(int1,'int1(mkea)=int2(mnea)trafo3(nk)',int2,yo,force_sync=.true.)
             call tensor_contract(X4,'X4(dkea)+=int1(Mkea)trafo2(Md)',int1,xv,force_sync=.true.)
             call tensor_free(int1)

             call tensor_ainit(yv, [blb,blv2], Ttype=TT_DenseTensor)
             call copy_block_from_full_matrix(trafo4%p%p,w1,BetaBatched,blb,V2Batched,blv2,nb,nv)
             call tensor_convert(w1,yv)
    
             call tensor_ainit( int1, [bla,blv2,nv,blv4], local=local, Ttype=TT_TiledDistributedTensor, &
                & tdim=[bs,vs,vs,vs],bg=use_bg_buf ) 
             call tensor_contract(int1,'int1(madb)=int2(mndb)trafo4(na)',int2,yv,force_sync=.true.)
             call tensor_contract(gvvvv,'gvvvv(cadb)+=int1(madb)trafo2(mc)',int1,xv,force_sync=.true.)

             call tensor_ainit(xo, [bla,no], Ttype=TT_DenseTensor)
             call copy_stripe_from_full_matrix(trafo1%p%p,w1,AlphaBatched,bla,nb,no)
             call tensor_convert(w1,xo)

             mtidx = [4,1,2,3]
             call tensor_contract(1.0E0_realk,int1,xo,[1],[1],1,1.0E0_realk,govvv,mtidx,force_sync=.true.)

             call tensor_free(int1)
             call tensor_free(int2)
             call tensor_free(Cint)
             call tensor_free(xo)
             call tensor_free(yo)
             call tensor_free(xv)
             call tensor_free(yv)

          enddo
       enddo

       enddo
    enddo

    ! Free integral stuff
    ! *******************
    mylsitem%setting%SCHEME%CS_SCREEN = save_cs_screen
    mylsitem%setting%SCHEME%PS_SCREEN = save_ps_screen

    if( use_bg_buf )then
       call mem_pseudo_dealloc( w1 )
    else
       call mem_dealloc( w1 )
    endif

  end subroutine get_4_batched_mo_int_par

end module ccsd_left_trans

!> ----------------------------------------------------------------
!> \brief The slave routine to calculate contributions to the left
!>        transformation residual
!> \author Dmytro Bykov
!> \start date   OCT 2015
!> ----------------------------------------------------------------
subroutine get_ccsd_multipliers_parallel_slave()
use tensor_interface_module
use typedeftype
use ccsd_left_trans
use decmpi_module

   implicit none
    
    !INPUT declarations
    !******************
    type(tensor)    :: m1,m2
    type(tensor)    :: t1,t2
    type(tensor)    :: govov
    type(tensor)    :: xo,yo,xv,yv
    type(tensor)    :: fo
    integer         :: no,nv,nb
    type(lsitem)    :: MyLsItem
    integer         :: iter 
    type(tensor)    :: rho1,rho2


#ifdef VAR_MPI
   call get_slaves_ccsd_multipliers_parallel(rho1,rho2,govov,&
                                        &t1,t2,m1,m2,&
                                        &xo,yo,xv,yv,&
                                        &fo,iter,&
                                        &no,nv,nb,&
                                        &MyLsItem)
#endif
   call get_ccsd_multipliers_parallel&
                                      &(rho1,rho2,&    !residuals 
                                      & govov,&        !integrals
                                      & t1,t2,m1,m2,&  !amps and mults
                                      & fo,&           !AO Fock matrix 
                                      & xo,yo,xv,yv,&  !transformation matricies
                                      & no,nv,nb,&     !number of occ, virt, basis
                                      & iter,&
                                      & MyLsItem)
end subroutine get_ccsd_multipliers_parallel_slave

