### USAGE

python molcas2dalton_moleculeInput.py dirInput/ dirOutput/

where dirInput/ is the directory containing the inputs (eg. MOLCAS molecule inputs with extension .xyz) 
 and  dirOutput/ is the directory containing the outputs (eg. DALTON molecule outputs with extension .mol)
