#!/usr/bin/env python

import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))
from runtest_lsdalton import Filter, TestRun

test = TestRun(__file__, sys.argv)

f = Filter()
f.add(string = 'Final HF energy',
      rel_tolerance = 1.0e-8)

#the individual errors are 1e-4 but they can accumulate
f.add(string = 'Correlation energy',
      rel_tolerance = 1.2e-3)

#Mostly determined by the HF energy
f.add(string = 'Total MP2 energy',
      rel_tolerance = 1.0e-5)

f.add(from_string = 'CORE LOCALITY',
      to_string   = 'Min. sigma_4 and orb.number',
      abs_tolerance = 1.0e-4)

f.add(from_string = 'VALENCE LOCALITY',
      to_string   = 'Min. sigma_4 and orb.number',
      abs_tolerance = 1.0e-4)

f.add(from_string = 'VIRTUAL LOCALITY',
      to_string   = 'Min. sigma_4 and orb.number',
      abs_tolerance = 1.0e-4)

#Using a FOT of 1.e-4!
f.add(from_string = 'MP2 occupied single energies',
      to_string   = 'MP2 Lagrangian correlation energy',
      abs_tolerance = 1.0e-4)

f.add(from_string = 'DEC-MP2 GRADIENT',
      abs_tolerance = 5.0e-4)

f.add(from_string = 'TOTAL MP2 MOLECULAR GRADIENT',
      num_lines   = 13,
      abs_tolerance = 5.0e-4)

f.add(from_string = 'Nuclear repulsion gradient',
      num_lines   = 13,
      abs_tolerance = 5.0e-4)

f.add(from_string = 'One-electron integral gradient',
      num_lines   = 13,
      abs_tolerance = 5.0e-4)

f.add(from_string = 'Reorthonormalization gradient',
      num_lines   = 13,
      abs_tolerance = 5.0e-4)

f.add(from_string = 'Two-electron integral gradient',
      num_lines   = 14,
      ignore_below = 1.0e-7,
      abs_tolerance = 5.0e-4)

f.add(string = 'Allocated memory (TOTAL)')

f.add(string = 'Memory in use for array4',
      rel_tolerance = 1.0e-9)

test.run(['decmp2_fragopt.dal'], ['StrangeString.mol'], {'out': f})

sys.exit(test.return_code)
