#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION                                                   #
#######################################################################
cat > LSDALTON_camb3lyp.info <<'%EOF%'
   LSDALTON_camb3lyp
   ------------------------
   Molecule:         5 HCN molecules placed 20 atomic units apart
   Wave Function:    CAMB3LYP/STO-2G
   Test Purpose:     test CAMB3LYP with default settings
%EOF%

#######################################################################
#  MOLECULE INPUT                                                     #
#######################################################################
cat > LSDALTON_camb3lyp.mol <<'%EOF%'
BASIS
STO-2G
5 HCN molecules placed 20 atomic units apart
STRUCTURE IS NOT OPTIMIZED. STO-2G basis 
Atomtypes=3 Nosymmetry
Charge=1. Atoms=5
H   0.0   0.0    -1.0 
H   0.0  20.0    -1.0 
H   0.0  40.0    -1.0 
H   0.0  60.0    -1.0 
H   0.0  80.0    -1.0 
Charge=7. Atoms=5
N   0.0   0.0     1.5
N   0.0  20.0     1.5
N   0.0  40.0     1.5
N   0.0  60.0     1.5
N   0.0  80.0     1.5
Charge=6. Atoms=5
C   0.0   0.0     0.0
C   0.0  20.0     0.0
C   0.0  40.0     0.0
C   0.0  60.0     0.0
C   0.0  80.0     0.0
%EOF%

#######################################################################
#  DALTON INPUT                                                       #
#######################################################################
cat > LSDALTON_camb3lyp.dal <<'%EOF%'
**GENERAL
.TESTMPICOPY
**WAVE FUNCTIONS
.DFT
 CAMB3LYP
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.START
TRILEVEL
.CONVDYN
TIGHT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT                                                       #
#######################################################################
echo $CHECK_SHELL > LSDALTON_camb3lyp.check
cat >> LSDALTON_camb3lyp.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY test
CRIT1=`$GREP "Final DFT energy\: * \-438\.0401477" $log | wc -l`
TEST[1]=`expr	$CRIT1`
CTRL[1]=2
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# SCF CONVERGENCE test level 2
CRIT1=`$GREP " 1 * \-437\.96466" $log | wc -l`
TEST[3]=`expr	$CRIT1`
CTRL[3]=2
ERROR[3]="DFT CONVERGENCE NOT CORRECT -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
